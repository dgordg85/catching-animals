package ru.kozyrev.spring.enumerate;

import org.apache.commons.lang.StringUtils;

public enum Status {

    ACTIVE("Активный"),
    DELETED("Удаленный");

    public String description;

    Status(final String description) {
        this.description = description;
    }

    public static Status getEnum(final String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (final Status v : values())
            if (v.toString().equals(value)) return v;
        throw new IllegalArgumentException();
    }
}
