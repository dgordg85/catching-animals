package ru.kozyrev.spring.enumerate;

import org.apache.commons.lang.StringUtils;

public enum Role {
    ADMIN("Администратор"),
    USER("Пользователь");

    public String description;

    Role(final String description) {
        this.description = description;
    }

    public static Role getEnum(final String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (final Role v : values())
            if (v.toString().equals(value)) return v;
        throw new IllegalArgumentException();
    }
}
