package ru.kozyrev.spring.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * Класс ошибок
 */
@Getter
@Setter
@ApiModel(description = "Модель ошибок")
public class ResponseError {

    /**
     * Уникальный идентификатор ошибки
     */
    @ApiModelProperty(value = "Идентификатор ошибки", required = true, example = "915a3da4-8a3c-4c75-b3c2-4af655d0fbb5")
    private UUID uuid = UUID.randomUUID();

    /**
     * Сообщение об ошибке
     */
    @ApiModelProperty(value = "Сообщение ошибки", required = true, example = "Data not found")
    private String message;

    /**
     * Код ошибки
     */
    @ApiModelProperty(value = "Код ошибки", required = true, example = "42")
    private String codeError;

    /**
     * Информация о системе
     */
    @ApiModelProperty(value = "Идентификатор системы", required = true, example = "Windows - 10.0")
    private String systemId;

    public ResponseError(final String message, final String codeError) {
        this.message = message;
        this.codeError = codeError;
        setSystemId();
    }

    public void setSystemId() {
        this.systemId = System.getProperty("os.name") + "-" + System.getProperty("os.version");
    }
}
