package ru.kozyrev.spring.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.spring.enumerate.Role;
import ru.kozyrev.spring.enumerate.Status;

import java.util.UUID;


/**
 * Класс DTO пользователя
 */
@Getter
@Setter
@ApiModel(description = "Модель DTO пользователя")
public class UserDTO {

    /**
     * Уникальный идентификатор пользователя
     */
    @NotNull
    @ApiModelProperty(value = "Уникальный идентификатор пользователя", required = true, example = "9a20d592-9eb8-4780-8a51-4c6f8d820e73")
    private UUID id;

    /**
     * Логни пользователя - уникальный
     */
    @NotNull
    @ApiModelProperty(value = "Логин пользователя", required = true, example = "AndreyC")
    private String username;

    /**
     * Пароль пользователя
     */
    @NotNull
    @ApiModelProperty(value = "Пароль", required = true, example = "kJ4d&sl3gka")
    private String password;

    /**
     * Роль пользователя в системе
     */
    @NotNull
    @ApiModelProperty(value = "Роль пользователя", required = true, allowableValues = "ADMIN, USER", example = "USER")
    private Role role;

    /**
     * Электронный почтовый ящик
     */
    @NotNull
    @ApiModelProperty(value = "Электронный ящик", required = true, example = "andrey@mail.ru")
    private String email;

    /**
     * Статус пользователя
     */
    @NotNull
    @ApiModelProperty(value = "Статус пользователя", required = true, allowableValues = "ACTIVE, DELETED", example = "ACTIVE")
    private Status status;
}