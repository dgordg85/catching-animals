package ru.kozyrev.spring.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Модель юзера для авторизации")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuthUserDTO {

    @ApiModelProperty(value = "Логин пользователя", required = true, example = "user1")
    private String login;

    @ApiModelProperty(value = "Пароль пользователя", required = true, example = "password1")
    private String password;

    @ApiModelProperty(value = "Система в которую нужен доступ", required = true, example = "ca-impl")
    private String audience;
}
