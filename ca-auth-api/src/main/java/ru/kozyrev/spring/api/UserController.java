package ru.kozyrev.spring.api;

import io.swagger.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.dto.UserDTO;

import java.util.UUID;

/**
 * Интерфейс контроллера пользователя
 */

@Api(value = "API работы с пользователем")
@RequestMapping("/api/user")
public interface UserController {
    /**
     * Метод добавления пользователя в базу
     *
     * @param userDTO пользователь для добавления в базу
     * @return ResponseEntity<UserDTO> со статусом CREATED
     */
    @NotNull
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Добавить пользователя", notes = "Позволяет добавить пользователя через передачу DTO объекта")
    @ApiResponse(code = 201, message = "Пользователь успешно добавлен")
    ResponseEntity<UserDTO> createUser(
            @ApiParam(required = true, value = "DTO пользователя")
            @Nullable @RequestBody UserDTO userDTO,
            @NotNull UriComponentsBuilder uriComponentsBuilder
    );

    /**
     * Метод получения пользователя по идентификатору
     *
     * @param id идентификатор пользователя
     * @return ResponseEntity<UserDTO> со статусом ОК
     */
    @Nullable
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Получить пользователя", notes = "Позволяет получить пользователя по UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Пользователь успешно получен"),
            @ApiResponse(code = 404, message = "Пользователь не найден")
    })
    ResponseEntity<UserDTO> getUser(
            @ApiParam(value = "Идентификатор пользователя", required = true, example = "3840439b-d048-45f1-8c83-8ec4a5c07f79")
            @Nullable @PathVariable UUID id
    );

    /**
     * Метод обновляет пользователя с определенным идентификатором
     *
     * @param userDTO пользователь для обновления в базе
     * @param id      идентификатор пользователя
     * @return ResponseEntity<UserDTO> со статусом ОК
     */
    @NotNull
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Обновить пользователя", notes = "Позволяет обновить пользователя через передачу DTO объекта, " +
            "UUID передаваемого польователя и параметр id должны совпадать")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Пользователь успешно обновлен"),
            @ApiResponse(code = 404, message = "Пользователь для обновления не найден")
    })
    ResponseEntity<UserDTO> updateUser(
            @ApiParam(value = "DTO объект пользователя", required = true)
            @Nullable @RequestBody UserDTO userDTO,
            @ApiParam(value = "Идентификатор пользователя", required = true, example = "53c7f8d8-f8b5-4087-842d-14af220d13fe")
            @Nullable @PathVariable UUID id
    );

    /**
     * Метод удаления пользователя по идентификатору
     * @param id идентификатор пользователя
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Удалить пользователя", notes = "Позволяет удалить пользователя по UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Пользователь успешно удален"),
            @ApiResponse(code = 404, message = "Пользователь не найден")
    })
    void deleteUser(
            @ApiParam(value = "Идентификатор пользователя", required = true, example = "2ba7ba82-abca-4e0e-89f9-a8c7825836f5")
            @Nullable @PathVariable UUID id
    );


}
