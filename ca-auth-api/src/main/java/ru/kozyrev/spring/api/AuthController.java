package ru.kozyrev.spring.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kozyrev.spring.dto.AuthUserDTO;
import ru.kozyrev.spring.dto.TokenDTO;

/**
 * Интерфейс взаимодействия с токеном
 */
@Api(value = "API получение токена")
@RequestMapping("/api/auth")
public interface AuthController {

    @PostMapping
    @ApiOperation(value = "Получить токен", notes = "Позволяет сгенерировать токен")
    ResponseEntity<TokenDTO> getToken(
            @ApiParam(required = true, value = "Auth DTO пользователя")
            @RequestBody AuthUserDTO authUserDTO
    );
}
