package ru.kozyrev.spring.util;

import org.jetbrains.annotations.Nullable;
import ru.kozyrev.spring.dto.AbstractDTO;
import ru.kozyrev.spring.exception.EmptyEntityException;
import ru.kozyrev.spring.exception.UpdateFailException;

import java.util.UUID;


public class EntityUtil {

    /**
     * Метод проверяет совпадают ли ID строки запроса и переданного entity
     *
     * @param entity передаваемый объект
     * @param id     идентификатор строки запроса
     * @throws UpdateFailException исключение выбрасывается если не совпадают id запроса и объекта
     */
    public static void checkDTOEntityUpdate(
            @Nullable final AbstractDTO entity,
            @Nullable final UUID id
    ) throws UpdateFailException, EmptyEntityException {
        if (entity == null) {
            throw new EmptyEntityException();
        }
        if (entity.getId() == null || id == null) {
            throw new EmptyEntityException();
        }
        if (entity.getId().compareTo(id) != 0) {
            throw new UpdateFailException();
        }
    }

    /**
     * Метод проверят DTO на null
     *
     * @param entity сущность DTO
     * @throws EmptyEntityException если сущность пустая выбрасывается исключение
     */
    public static void checkDTONotEmpty(
            @Nullable final AbstractDTO entity
    ) throws EmptyEntityException {
        if (entity == null) {
            throw new EmptyEntityException();
        }
    }

}
