package ru.kozyrev.spring.schedule;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.kozyrev.spring.dto.search.AnimalSearchDTO;
import ru.kozyrev.spring.service.AnimalService;

@Component
@ConditionalOnProperty(prefix = "scheduling.count.animals", name = {"enabled"}, matchIfMissing = false)
@Slf4j
public class TimeSchedule {

    AnimalService animalService;

    public TimeSchedule(final AnimalService animalService) {
        this.animalService = animalService;
    }

    @Scheduled(fixedDelayString = "${scheduling.count.animals.interval}")
    public void schedule() {

        final long count = animalService.getAll(new AnimalSearchDTO(), Pageable.unpaged()).getTotalElements();
        log.info(String.format("Количество животных в базе: %d", count));
    }
}
