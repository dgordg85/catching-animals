package ru.kozyrev.spring.service;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kozyrev.spring.dto.ProposalDTO;
import ru.kozyrev.spring.dto.search.PageDTO;
import ru.kozyrev.spring.dto.search.ProposalSearchDTO;
import ru.kozyrev.spring.entity.Proposal;
import ru.kozyrev.spring.exception.EmptyException;
import ru.kozyrev.spring.repository.ProposalRepository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Сервис заявок
 */
@Service
public class ProposalService {

    @NotNull
    ProposalRepository repository;

    @NotNull
    MapperFacade mapperFacade;

    public ProposalService(
            @NotNull final ProposalRepository repository,
            @NotNull final MapperFactory mapperFactory
    ) {
        this.repository = repository;
        this.mapperFacade = mapperFactory.getMapperFacade();
    }

    @Transactional
    public void delete(@Nullable final UUID id) {
        if (id == null) {
            throw new EmptyException();
        }
        final Proposal proposal = new Proposal();
        proposal.setId(id);
        repository.delete(proposal);
    }

    @Nullable
    @Transactional(readOnly = true)
    public ProposalDTO getOne(@Nullable final UUID id) {
        if (id == null) {
            throw new EmptyException();
        }
        final Proposal proposal = repository.getOne(id);
        return mapperFacade.map(proposal, ProposalDTO.class);
    }

    @NotNull
    @Transactional(readOnly = true)
    public PageDTO<ProposalDTO> getAll(
            @Nullable final ProposalSearchDTO proposalSearchDTO,
            @NotNull final Pageable pageable
    ) {
        if (proposalSearchDTO == null) {
            throw new EmptyException();
        }
        final Page<Proposal> page = repository.findAll(getSpec(proposalSearchDTO), pageable);
        final List<ProposalDTO> list = getProposalsListDTO(page.toList());
        return new PageDTO<>(
                list,
                pageable.getPageNumber(),
                pageable.getPageSize(),
                page.getTotalElements()
        );
    }

    @NotNull
    @Transactional
    public ProposalDTO update(@NotNull final ProposalDTO proposalDTO) {
        final Proposal proposalUpdate = mapperFacade.map(proposalDTO, Proposal.class);
        final Proposal result = repository.save(proposalUpdate);
        return mapperFacade.map(result, ProposalDTO.class);
    }

    @NotNull
    @Transactional
    public ProposalDTO add(@NotNull final ProposalDTO proposalDTO) {
        final Proposal proposal = mapperFacade.map(proposalDTO, Proposal.class);
        final Proposal result = repository.save(proposal);
        return mapperFacade.map(result, ProposalDTO.class);
    }

    @NotNull
    private List<ProposalDTO> getProposalsListDTO(@NotNull final List<Proposal> proposals) {
        final List<ProposalDTO> proposalsDTO = new ArrayList<>();
        for (final Proposal proposal : proposals) {
            final ProposalDTO proposalDTO = mapperFacade.map(proposal, ProposalDTO.class);
            proposalsDTO.add(proposalDTO);
        }
        return proposalsDTO;
    }

    private Specification<Proposal> getSpec(@NotNull final ProposalSearchDTO dto) {
        return (root, query, criteriaBuilder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            if (dto.getName() != null) {
                predicates.add(root.get("name").in(dto.getName()));
            }
            if (dto.getDescription() != null) {
                predicates.add(root.get("description").in(dto.getDescription()));
            }
            if (dto.getIsContract() != null) {
                predicates.add(root.get("isContract").in(dto.getIsContract()));
            }
            if (dto.getDistrict() != null) {
                predicates.add(root.get("district").as(String.class).in(dto.getDistrict()));
            }
            return criteriaBuilder.and(predicates.toArray(Predicate[]::new));
        };
    }
}
