package ru.kozyrev.spring.service;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kozyrev.spring.dto.EventDTO;
import ru.kozyrev.spring.dto.search.EventSearchDTO;
import ru.kozyrev.spring.dto.search.PageDTO;
import ru.kozyrev.spring.entity.Event;
import ru.kozyrev.spring.exception.EmptyException;
import ru.kozyrev.spring.repository.EventRepository;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Сервис событий
 */
@Service
public class EventService {

    @NotNull
    EventRepository repository;

    @NotNull
    MapperFacade mapperFacade;

    public EventService(
            @NotNull final EventRepository repository,
            @NotNull final MapperFactory mapperFactory
    ) {
        this.repository = repository;

        this.mapperFacade = mapperFactory.getMapperFacade();
    }

    @Transactional
    public void delete(@Nullable final UUID id) {
        if (id == null) {
            throw new EmptyException();
        }
        final Event event = new Event();
        event.setId(id);
        repository.delete(event);
    }

    @Nullable
    @Transactional(readOnly = true)
    public EventDTO getOne(@Nullable final UUID id) {
        if (id == null) {
            throw new EmptyException();
        }
        final Event event = repository.getOne(id);
        return mapperFacade.map(event, EventDTO.class);
    }

    @NotNull
    @Transactional(readOnly = true)
    public PageDTO<EventDTO> getAll(
            @Nullable final EventSearchDTO eventSearchDTO,
            @NotNull final Pageable pageable
    ) {
        if (eventSearchDTO == null) {
            throw new EmptyException();
        }
        final Page<Event> page = repository.findAll(getSpec(eventSearchDTO), pageable);
        final List<EventDTO> list = getEventsListDTO(page.toList());
        return new PageDTO<>(
                list,
                pageable.getPageNumber(),
                pageable.getPageSize(),
                page.getTotalElements()
        );
    }

    @NotNull
    @Transactional
    public EventDTO update(@NotNull final EventDTO eventDTO) {
        final Event eventUpdate = mapperFacade.map(eventDTO, Event.class);
        final Event result = repository.save(eventUpdate);
        return mapperFacade.map(result, EventDTO.class);
    }

    @NotNull
    @Transactional
    public EventDTO add(@NotNull final EventDTO eventDTO) {
        final Event event = mapperFacade.map(eventDTO, Event.class);
        final Event result = repository.save(event);
        return mapperFacade.map(result, EventDTO.class);
    }

    @NotNull
    private List<EventDTO> getEventsListDTO(@NotNull final List<Event> events) {
        final List<EventDTO> eventsListDTO = new ArrayList<>();
        for (final Event event : events) {
            final EventDTO eventDTO = mapperFacade.map(event, EventDTO.class);
            eventsListDTO.add(eventDTO);
        }
        return eventsListDTO;
    }

    private Specification<Event> getSpec(@NotNull final EventSearchDTO dto) {
        return (root, query, criteriaBuilder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            if (dto.getType() != null) {
                predicates.add(root.get("type").as(String.class).in(dto.getType()));
            }
            if (dto.getAnimalId() != null) {
                predicates.add(criteriaBuilder.equal(root.get("animal").get("id"), dto.getAnimalId()));
            }
            final LocalDateTime after = dto.getDateAfter();
            final LocalDateTime before = dto.getDateBefore();
            if (after != null && before == null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("date"), after));
            }
            if (after == null && before != null) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("date"), before));
            }
            if (after != null && before != null) {
                predicates.add(criteriaBuilder.between(root.get("date"), after, before));
            }
            return criteriaBuilder.and(predicates.toArray(Predicate[]::new));
        };
    }
}
