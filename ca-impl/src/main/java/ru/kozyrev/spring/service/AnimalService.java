package ru.kozyrev.spring.service;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kozyrev.spring.dto.AnimalDTO;
import ru.kozyrev.spring.dto.search.AnimalSearchDTO;
import ru.kozyrev.spring.dto.search.PageDTO;
import ru.kozyrev.spring.entity.Animal;
import ru.kozyrev.spring.exception.EmptyException;
import ru.kozyrev.spring.repository.AnimalRepository;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Сервис животных
 */
@Service
public class AnimalService {

    @NotNull
    AnimalRepository repository;

    @NotNull
    MapperFacade mapperFacade;

    public AnimalService(
            @NotNull final AnimalRepository repository,
            @NotNull final MapperFactory mapperFactory) {
        this.repository = repository;
        this.mapperFacade = mapperFactory.getMapperFacade();
    }

    @Transactional
    public void delete(@Nullable final UUID id) {
        if (id == null) {
            throw new EmptyException();
        }
        @Nullable final Animal animal = new Animal();
        animal.setId(id);
        repository.delete(animal);
    }

    @NotNull
    @Transactional(readOnly = true)
    public AnimalDTO getOne(@Nullable final UUID id) {
        if (id == null) {
            throw new EmptyException();
        }
        @NotNull final Animal animal = repository.getOne(id);
        return mapperFacade.map(animal, AnimalDTO.class);
    }

    @NotNull
    @Transactional(readOnly = true)
    public PageDTO<AnimalDTO> getAll(
            @Nullable final AnimalSearchDTO animalSearchDTO,
            @NotNull final Pageable pageable
    ) {
        if (animalSearchDTO == null) {
            throw new EmptyException();
        }
        final Page<Animal> page = repository.findAll(getSpec(animalSearchDTO), pageable);
        final List<AnimalDTO> list = getAnimalsListDTO(page.toList());
        return new PageDTO<>(
                list,
                pageable.isPaged() ? pageable.getPageNumber() : 0,
                pageable.isPaged() ? pageable.getPageSize() : 0,
                page.getTotalElements()
        );
    }

    @NotNull
    @Transactional
    public AnimalDTO update(@NotNull final AnimalDTO animalDTO) {
        final Animal animalUpdate = mapperFacade.map(animalDTO, Animal.class);
        final Animal result = repository.save(animalUpdate);
        return mapperFacade.map(result, AnimalDTO.class);
    }

    @Transactional
    @NotNull
    public AnimalDTO add(@NotNull final AnimalDTO animalDTO) {
        final Animal animalAdd = mapperFacade.map(animalDTO, Animal.class);
        final Animal result = repository.save(animalAdd);
        return mapperFacade.map(result, AnimalDTO.class);
    }

    @NotNull
    private List<AnimalDTO> getAnimalsListDTO(@NotNull final List<Animal> animals) {
        final List<AnimalDTO> animalsDTO = new ArrayList<>();
        for (final Animal animal : animals) {
            final AnimalDTO animalDTO = mapperFacade.map(animal, AnimalDTO.class);
            animalsDTO.add(animalDTO);
        }
        return animalsDTO;
    }

    private Specification<Animal> getSpec(@NotNull final AnimalSearchDTO dto) {
        return (root, query, criteriaBuilder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            if (dto.getName() != null) {
                predicates.add(root.get("name").in(dto.getName()));
            }
            if (dto.getGender() != null) {
                predicates.add(root.get("gender").as(String.class).in(dto.getGender()));
            }
            if (dto.getDescription() != null) {
                predicates.add(root.get("description").in(dto.getDescription()));
            }
            if (dto.getType() != null) {
                predicates.add(root.get("type").as(String.class).in(dto.getType()));
            }
            if (dto.getOwnerId() != null) {
                predicates.add(criteriaBuilder.equal(root.get("owner").get("id"), dto.getOwnerId()));
            }
            if (dto.getProposalId() != null) {
                predicates.add(criteriaBuilder.equal(root.get("proposal").get("id"), dto.getProposalId()));
            }
            final LocalDate after = dto.getBirthdayAfter();
            final LocalDate before = dto.getBirthdayBefore();
            if (after != null && before == null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("birthday"), after));
            }
            if (after == null && before != null) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("birthday"), before));
            }
            if (after != null && before != null) {
                predicates.add(criteriaBuilder.between(root.get("birthday"), after, before));
            }
            return criteriaBuilder.and(predicates.toArray(Predicate[]::new));
        };
    }
}
