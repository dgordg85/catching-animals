package ru.kozyrev.spring.service;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kozyrev.spring.dto.OwnerDTO;
import ru.kozyrev.spring.dto.search.OwnerSearchDTO;
import ru.kozyrev.spring.dto.search.PageDTO;
import ru.kozyrev.spring.entity.Owner;
import ru.kozyrev.spring.exception.EmptyException;
import ru.kozyrev.spring.repository.OwnerRepository;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Сервис владельцев
 */
@Service
public class OwnerService {

    @NotNull
    OwnerRepository repository;

    @NotNull
    MapperFacade mapperFacade;

    public OwnerService(
            @NotNull final OwnerRepository repository,
            @NotNull final MapperFactory mapperFactory
    ) {
        this.repository = repository;
        this.mapperFacade = mapperFactory.getMapperFacade();
    }

    @Transactional
    public void delete(@Nullable final UUID id) {
        if (id == null) {
            throw new EmptyException();
        }
        final Owner owner = new Owner();
        owner.setId(id);
        repository.delete(owner);
    }

    @Nullable
    @Transactional(readOnly = true)
    public OwnerDTO getOne(@Nullable final UUID id) {
        if (id == null) {
            throw new EmptyException();
        }
        final Owner owner = repository.getOne(id);
        return mapperFacade.map(owner, OwnerDTO.class);
    }

    @NotNull
    @Transactional(readOnly = true)
    public PageDTO<OwnerDTO> getAll(
            @Nullable final OwnerSearchDTO ownerSearchDTO,
            @NotNull final Pageable pageable
    ) {
        if (ownerSearchDTO == null) {
            throw new EmptyException();
        }
        final Page<Owner> page = repository.findAll(getSpec(ownerSearchDTO), pageable);
        final List<OwnerDTO> list = getOwnersListDTO(page.toList());
        return new PageDTO<>(
                list,
                pageable.getPageNumber(),
                pageable.getPageSize(),
                page.getTotalElements()
        );
    }

    @NotNull
    @Transactional
    public OwnerDTO update(@NotNull final OwnerDTO ownerDTO) {
        final Owner ownerUpdate = mapperFacade.map(ownerDTO, Owner.class);
        final Owner result = repository.save(ownerUpdate);
        return mapperFacade.map(result, OwnerDTO.class);
    }

    @NotNull
    @Transactional
    public OwnerDTO add(@NotNull final OwnerDTO ownerDTO) {
        final Owner owner = mapperFacade.map(ownerDTO, Owner.class);
        final Owner result = repository.save(owner);
        return mapperFacade.map(result, OwnerDTO.class);
    }

    @NotNull
    private List<OwnerDTO> getOwnersListDTO(@NotNull final List<Owner> owners) {
        final List<OwnerDTO> ownersDTO = new ArrayList<>();
        for (final Owner owner : owners) {
            final OwnerDTO ownerDTO = mapperFacade.map(owner, OwnerDTO.class);
            ownersDTO.add(ownerDTO);
        }
        return ownersDTO;
    }

    private Specification<Owner> getSpec(@NotNull final OwnerSearchDTO dto) {
        return (root, query, criteriaBuilder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            if (dto.getName() != null) {
                predicates.add(root.get("name").in(dto.getName()));
            }
            if (dto.getAddress() != null) {
                predicates.add(root.get("address").in(dto.getAddress()));
            }
            if (dto.getPhone() != null) {
                predicates.add(root.get("phone").in(dto.getPhone()));
            }
            return criteriaBuilder.and(predicates.toArray(Predicate[]::new));
        };
    }
}
