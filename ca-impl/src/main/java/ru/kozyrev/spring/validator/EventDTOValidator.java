package ru.kozyrev.spring.validator;

import ru.kozyrev.spring.dto.EventDTO;
import ru.kozyrev.spring.exception.EmptyException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Locale;

@Component
public class EventDTOValidator implements Validator {

    private final MessageSource messageSource;

    public EventDTOValidator(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public boolean supports(@NotNull final Class<?> clazz) {
        return EventDTO.class.equals(clazz);
    }

    @Override
    public void validate(@Nullable final Object target, @NotNull final Errors errors) {
        if (target == null) {
            final String message = messageSource.getMessage("entity.empty", new Object[]{}, Locale.getDefault());
            throw new EmptyException(message);
        }

        final EventDTO eventDTO = (EventDTO) target;

        if (eventDTO.getAnimalId() == null) {
            final String message = messageSource.getMessage("event.animalId.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("animal", "event.animalId.empty", message);
        }
        if (eventDTO.getType() == null) {
            final String message = messageSource.getMessage("event.type.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("type", "event.type.empty", message);
        }
        if (eventDTO.getDate() == null) {
            final String message = messageSource.getMessage("event.date.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("date", "event.date.empty", message);
        }
    }
}
