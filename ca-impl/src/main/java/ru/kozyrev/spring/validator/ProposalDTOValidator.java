package ru.kozyrev.spring.validator;

import org.apache.commons.lang.StringUtils;
import ru.kozyrev.spring.dto.ProposalDTO;
import ru.kozyrev.spring.exception.EmptyException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Locale;

@Component
public class ProposalDTOValidator implements Validator {

    private final MessageSource messageSource;

    public ProposalDTOValidator(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public boolean supports(@NotNull final Class<?> clazz) {
        return ProposalDTO.class.equals(clazz);
    }

    @Override
    public void validate(@Nullable final Object target, @NotNull final Errors errors) {
        if (target == null) {
            final String message = messageSource.getMessage("entity.empty", new Object[]{}, Locale.getDefault());
            throw new EmptyException(message);
        }

        final ProposalDTO proposalDTO = (ProposalDTO) target;

        if (StringUtils.isBlank(proposalDTO.getName())) {
            final String message = messageSource.getMessage("proposal.name.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("name", "proposal.name.empty", message);
        }

        if (StringUtils.isBlank(proposalDTO.getDescription())) {
            final String message = messageSource.getMessage("proposal.description.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("description", "proposal.description.empty", message);
        }
        if (proposalDTO.getDistrict() == null) {
            final String message = messageSource.getMessage("proposal.district.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("district", "proposal.district.empty", message);
        }
        if (proposalDTO.getIsContract() == null) {
            final String message = messageSource.getMessage("proposal.contract.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("contract", "proposal.contract.empty", message);
        }
    }
}
