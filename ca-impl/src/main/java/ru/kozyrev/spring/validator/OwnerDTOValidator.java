package ru.kozyrev.spring.validator;

import org.apache.commons.lang.StringUtils;
import ru.kozyrev.spring.dto.OwnerDTO;
import ru.kozyrev.spring.exception.EmptyException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Locale;

@Component
public class OwnerDTOValidator implements Validator {

    private final MessageSource messageSource;

    public OwnerDTOValidator(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public boolean supports(@NotNull final Class<?> clazz) {
        return OwnerDTO.class.equals(clazz);
    }

    @Override
    public void validate(@Nullable final Object target, @NotNull final Errors errors) {
        if (target == null) {
            final String message = messageSource.getMessage("entity.empty", new Object[]{}, Locale.getDefault());
            throw new EmptyException(message);
        }

        final OwnerDTO ownerDTO = (OwnerDTO) target;

        if (StringUtils.isBlank(ownerDTO.getName())) {
            final String message = messageSource.getMessage("owner.name.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("name", "owner.name.empty", message);
        }
    }
}
