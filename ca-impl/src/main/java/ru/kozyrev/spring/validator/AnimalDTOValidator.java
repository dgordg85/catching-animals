package ru.kozyrev.spring.validator;

import org.apache.commons.lang.StringUtils;
import ru.kozyrev.spring.dto.AnimalDTO;
import ru.kozyrev.spring.exception.EmptyException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Locale;

/**
 * Класс валидации AnimalDTO
 */
@Component
public class AnimalDTOValidator implements Validator {

    @NotNull
    private final MessageSource messageSource;

    public AnimalDTOValidator(@NotNull final MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public boolean supports(@NotNull final Class<?> clazz) {
        return AnimalDTO.class.equals(clazz);
    }

    @Override
    public void validate(@Nullable final Object target, @NotNull final Errors errors) {
        if (target == null) {
            final String message = messageSource.getMessage("entity.empty", new Object[]{}, Locale.getDefault());
            throw new EmptyException(message);
        }

        final AnimalDTO animalDTO = (AnimalDTO) target;

        if (StringUtils.isBlank(animalDTO.getName())) {
            final String message = messageSource.getMessage("animal.name.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("name", "animal.name.empty", message);
        }
        if (animalDTO.getGender() == null) {
            final String message = messageSource.getMessage("animal.gender.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("gender", "animal.gender.empty", message);
        }
        if (animalDTO.getBirthday() == null) {
            final String message = messageSource.getMessage("animal.birthday.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("birthday", "animal.birthday.empty", message);
        }
        if (StringUtils.isBlank(animalDTO.getDescription())) {
            final String message = messageSource.getMessage("animal.description.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("description", "animal.description.empty", message);
        }
        if (animalDTO.getType() == null) {
            final String message = messageSource.getMessage("animal.type.empty", new Object[]{}, Locale.getDefault());
            errors.rejectValue("type", "animal.type.empty", message);
        }
    }
}
