package ru.kozyrev.spring.listener;

import ru.kozyrev.spring.entity.AbstractEntity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.Instant;

public class AbstractEntityListener {


    @PrePersist
    private void setCreateAt(final AbstractEntity abstractEntity) {
        abstractEntity.setCreatedAt(Instant.now());
        abstractEntity.setUpdatedAt(abstractEntity.getCreatedAt());
    }

    @PreUpdate
    private void setUpdateAt(final AbstractEntity abstractEntity) {
        abstractEntity.setUpdatedAt(Instant.now());
    }

}
