package ru.kozyrev.spring.exception;

/**
 * исключение выбрасывается при пустом запросе
 */
public class EmptyException extends RuntimeException {

    public EmptyException() {
        super("Пустой запрос");
    }

    public EmptyException(final String message) {
        super(message);
    }
}
