package ru.kozyrev.spring.exception;

/**
 * исключение выбрасывается, если по переданному UUID внутри Entity - в базе не найдено сущности для привязки
 * или в передаваемой сущности отсутствует требуемый UUID
 */
public class EmptyEntityException extends RuntimeException {

    public EmptyEntityException() {
        super("Сущность не найдена!");
    }

    public EmptyEntityException(final String message) {
        super(message);
    }
}
