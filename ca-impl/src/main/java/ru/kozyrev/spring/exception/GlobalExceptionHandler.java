package ru.kozyrev.spring.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.kozyrev.spring.dto.ResponseError;

import javax.persistence.EntityNotFoundException;

@RestControllerAdvice(basePackages = "ru.kozyrev.spring.controller")
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EmptyEntityException.class)
    public ResponseEntity<ResponseError> emptyEntityException(final EmptyEntityException e) {
        final ResponseError responseError = new ResponseError(
                e.getLocalizedMessage(),
                "EmptyEntityException");
        return new ResponseEntity<>(responseError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmptyException.class)
    public ResponseEntity<ResponseError> emptyException(final EmptyException e) {
        final ResponseError responseError = new ResponseError(
                e.getLocalizedMessage(),
                "EmptyException");
        return new ResponseEntity<>(responseError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UpdateFailException.class)
    public ResponseEntity<ResponseError> updateFailException(final UpdateFailException e) {
        final ResponseError responseError = new ResponseError(
                e.getLocalizedMessage(),
                "UpdateFailException");
        return new ResponseEntity<>(responseError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ResponseError> entityNotFoundException(final EntityNotFoundException e) {
        final ResponseError responseError = new ResponseError(
                e.getLocalizedMessage(),
                "EntityNotFoundException");
        return new ResponseEntity<>(responseError, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ResponseError> runtimeException(final RuntimeException e) {
        final ResponseError responseError = new ResponseError(
                e.getLocalizedMessage(),
                "RuntimeException");
        return new ResponseEntity<>(responseError, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
