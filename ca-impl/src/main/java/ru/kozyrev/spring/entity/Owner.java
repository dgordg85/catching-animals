package ru.kozyrev.spring.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Класс владельцев животных
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
public class Owner extends AbstractEntity {

    /**
     * Поле имя владельца
     */
    @NotNull
    @Column(nullable = false)
    private String name;

    /**
     * Поле телефона владельца
     */
    @Nullable
    private String phone;

    /**
     * Поле адреса владельца
     */
    @Nullable
    private String address;
}
