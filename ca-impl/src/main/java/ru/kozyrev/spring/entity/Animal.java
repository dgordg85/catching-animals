package ru.kozyrev.spring.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.spring.enumerate.AnimalType;
import ru.kozyrev.spring.enumerate.Gender;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Абстрактный класс животных
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
public class Animal extends AbstractEntity {

    /**
     * Поле кличка
     */
    @NotNull
    @Column(nullable = false)
    private String name;

    /**
     * Поле пол
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Gender gender;

    /**
     * Поле возраст
     */
    @NotNull
    @Column(nullable = false)
    private LocalDate birthday;

    /**
     * Поле описание животного
     */
    @NotNull
    @Column(nullable = false)
    private String description;

    /**
     * Поле типа животного
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private AnimalType type;

    /**
     * Поле владелец животного
     */
    @Nullable
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Owner owner;

    /**
     * Поле принадлежности к заявке
     */
    @Nullable
    @ManyToOne
    @JoinColumn(name = "proposal_id")
    private Proposal proposal;
}
