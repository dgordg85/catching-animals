package ru.kozyrev.spring.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.spring.enumerate.EventType;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Класс событий
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
public class Event extends AbstractEntity {

    /**
     * Поле животное
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "animal_id", nullable = false)
    private Animal animal;

    /**
     * Поле типа события
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EventType type;

    /**
     * Поле времени наступления события
     */
    @NotNull
    @Column(nullable = false)
    private LocalDateTime date;
}
