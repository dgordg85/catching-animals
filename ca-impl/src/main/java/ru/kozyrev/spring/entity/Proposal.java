package ru.kozyrev.spring.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.spring.enumerate.District;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Класс заявок
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Proposal extends AbstractEntity {

    /**
     * Поле название заявки
     */
    @NotNull
    @Column(nullable = false)
    private String name;

    /**
     * Поле описание заявки
     */
    @NotNull
    @Column(nullable = false)
    private String description;

    /**
     * Поле района (области) заявки
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private District district;

    /**
     * Поле является ли заявка контрактом
     */
    @NotNull
    @Column(name = "contract", nullable = false)
    private Boolean isContract = false;
}
