package ru.kozyrev.spring.controller;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.annotation.Audit;
import ru.kozyrev.spring.api.ProposalController;
import ru.kozyrev.spring.dto.ProposalDTO;
import ru.kozyrev.spring.dto.search.PageDTO;
import ru.kozyrev.spring.dto.search.ProposalSearchDTO;
import ru.kozyrev.spring.enumerate.AuditCode;
import ru.kozyrev.spring.service.ProposalService;
import ru.kozyrev.spring.util.EntityUtil;
import ru.kozyrev.spring.validator.ProposalDTOValidator;

import java.net.URI;
import java.util.UUID;

/**
 * Контроллер для доступа к заявкам
 */
@RestController
@Slf4j
public class ProposalControllerImpl implements ProposalController {

    private final ProposalService proposalService;

    private final ProposalDTOValidator proposalDTOValidator;

    public ProposalControllerImpl(
            final ProposalService proposalService,
            final ProposalDTOValidator proposalDTOValidator
    ) {
        this.proposalService = proposalService;
        this.proposalDTOValidator = proposalDTOValidator;
    }

    @NotNull
    @Override
    public PageDTO<ProposalDTO> getAllProposals(
            @Nullable final ProposalSearchDTO proposalSearchDTO,
            @NotNull final Pageable pageable
    ) {
        log.debug("getAllProposals with {} - start ", proposalSearchDTO);
        final PageDTO<ProposalDTO> result = proposalService.getAll(proposalSearchDTO, pageable);
        log.debug("getAllProposals end with result {}", result);
        return result;
    }

    @Override
    @Nullable
    public ProposalDTO getProposal(@Nullable final UUID id) {
        log.debug("getProposal with {} - start ", id);
        final ProposalDTO result = proposalService.getOne(id);
        log.debug("getProposal end with result {}", result);
        return result;
    }

    @Audit(AuditCode.CREATE_PROPOSAL)
    @Override
    @NotNull
    public ResponseEntity<ProposalDTO> putProposal(
            @Nullable final ProposalDTO proposalDTO,
            @NotNull final UriComponentsBuilder componentsBuilder,
            @NotNull final BindingResult bindingResult
    ) {
        log.debug("putProposal with {} - start ", proposalDTO);
        EntityUtil.checkDTONotEmpty(proposalDTO);
        proposalDTOValidator.validate(proposalDTO, bindingResult);
        if (bindingResult.hasErrors()) {
            proposalDTO.setErrors(bindingResult.getAllErrors());
            return new ResponseEntity<>(proposalDTO, HttpStatus.BAD_REQUEST);
        }
        final ProposalDTO addedProposalDTO = proposalService.add(proposalDTO);
        final URI uri = componentsBuilder.path("/api/proposal/" + addedProposalDTO.getId())
                .buildAndExpand(addedProposalDTO)
                .toUri();
        final ResponseEntity<ProposalDTO> result = ResponseEntity.created(uri).body(addedProposalDTO);
        log.debug("putProposal end with result {}", result);
        return result;
    }

    @Audit(AuditCode.UPDATE_PROPOSAL)
    @Override
    @NotNull
    public ResponseEntity<ProposalDTO> updateProposal(
            @Nullable final ProposalDTO proposalDTO,
            @Nullable final UUID id,
            @NotNull final BindingResult bindingResult
    ) {
        log.debug("updateProposal with {} - start ", proposalDTO);
        EntityUtil.checkDTOEntityUpdate(proposalDTO, id);
        proposalDTOValidator.validate(proposalDTO, bindingResult);
        if (bindingResult.hasErrors()) {
            proposalDTO.setErrors(bindingResult.getAllErrors());
            return new ResponseEntity<>(proposalDTO, HttpStatus.BAD_REQUEST);
        }
        final ProposalDTO updatedProposalDTO = proposalService.update(proposalDTO);

        final ResponseEntity<ProposalDTO> result = new ResponseEntity<>(updatedProposalDTO, HttpStatus.OK);
        log.debug("updateProposal end with result {}", result);
        return result;
    }

    @Audit(AuditCode.DELETE_PROPOSAL)
    @Override
    public void deleteProposal(@Nullable final UUID id) {
        log.debug("deleteProposal with {} - start ", id);
        proposalService.delete(id);
        log.debug("deleteProposal end with {}", id);
    }

    @ModelAttribute
    public ProposalDTO proposalDTO() {
        return new ProposalDTO();
    }

    @InitBinder(value = "proposalDTO")
    private void initBinder(final WebDataBinder binder) {
        binder.setValidator(proposalDTOValidator);
    }
}
