package ru.kozyrev.spring.controller;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.annotation.Audit;
import ru.kozyrev.spring.api.EventController;
import ru.kozyrev.spring.dto.EventDTO;
import ru.kozyrev.spring.dto.search.EventSearchDTO;
import ru.kozyrev.spring.dto.search.PageDTO;
import ru.kozyrev.spring.enumerate.AuditCode;
import ru.kozyrev.spring.service.EventService;
import ru.kozyrev.spring.util.EntityUtil;
import ru.kozyrev.spring.validator.EventDTOValidator;

import java.net.URI;
import java.util.UUID;

/**
 * Конроллер для доступа к событиям
 */
@RestController
@Slf4j
public class EventControllerImpl implements EventController {

    private final EventService eventService;

    private final EventDTOValidator eventDTOValidator;

    public EventControllerImpl(
            final EventService eventService,
            final EventDTOValidator eventDTOValidator
    ) {
        this.eventService = eventService;
        this.eventDTOValidator = eventDTOValidator;
    }

    @NotNull
    @Override
    public PageDTO<EventDTO> getAllEvents(
            @Nullable final EventSearchDTO eventSearchDTO,
            @NotNull final Pageable pageable
    ) {
        log.debug("getAllEvents with {} - start ", eventSearchDTO);
        final PageDTO<EventDTO> result = eventService.getAll(eventSearchDTO, pageable);
        log.debug("getAllEvents end with result {}", result);
        return result;
    }

    @Override
    @Nullable
    public EventDTO getEvent(@Nullable final UUID id) {
        log.debug("getEvent with {} - start ", id);
        final EventDTO result = eventService.getOne(id);
        log.debug("getEvent end with result {}", result);
        return result;
    }

    @Audit(AuditCode.CREATE_EVENT)
    @Override
    @NotNull
    public ResponseEntity<EventDTO> putEvent(
            @Nullable final EventDTO eventDTO,
            @NotNull final UriComponentsBuilder componentsBuilder,
            @NotNull final BindingResult bindingResult
    ) {
        log.debug("putEvent with {} - start ", eventDTO);
        EntityUtil.checkDTONotEmpty(eventDTO);
        eventDTOValidator.validate(eventDTO, bindingResult);
        if (bindingResult.hasErrors()) {
            eventDTO.setErrors(bindingResult.getAllErrors());
            return new ResponseEntity<>(eventDTO, HttpStatus.BAD_REQUEST);
        }
        final EventDTO addedEventDTO = eventService.add(eventDTO);
        final URI uri = componentsBuilder.path("/api/event/" + addedEventDTO.getId())
                .buildAndExpand(addedEventDTO)
                .toUri();
        final ResponseEntity<EventDTO> result = ResponseEntity.created(uri).body(addedEventDTO);
        log.debug("putEvent end with result {}", result);
        return result;
    }

    @Audit(AuditCode.UPDATE_EVENT)
    @Override
    @NotNull
    public ResponseEntity<EventDTO> updateEvent(
            @Nullable final EventDTO eventDTO,
            @Nullable final UUID id,
            @NotNull final BindingResult bindingResult
    ) {
        log.debug("updateEvent with {} - start ", eventDTO);
        EntityUtil.checkDTOEntityUpdate(eventDTO, id);
        eventDTOValidator.validate(eventDTO, bindingResult);
        if (bindingResult.hasErrors()) {
            eventDTO.setErrors(bindingResult.getAllErrors());
            return new ResponseEntity<>(eventDTO, HttpStatus.BAD_REQUEST);
        }
        final EventDTO updatedEventDTO = eventService.update(eventDTO);
        final ResponseEntity<EventDTO> result = new ResponseEntity<>(updatedEventDTO, HttpStatus.OK);
        log.debug("updateEvent end with result {}", result);
        return result;
    }

    @Audit(AuditCode.DELETE_EVENT)
    @Override
    public void deleteEvent(@Nullable final UUID id) {
        log.debug("deleteEvent with {} - start ", id);
        eventService.delete(id);
        log.debug("deleteEvent end with {}", id);
    }

    @ModelAttribute
    public EventDTO eventDTO() {
        return new EventDTO();
    }

    @InitBinder(value = "eventDTO")
    private void initBinder(final WebDataBinder binder) {
        binder.setValidator(eventDTOValidator);
    }
}
