package ru.kozyrev.spring.controller;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.annotation.Audit;
import ru.kozyrev.spring.api.AnimalController;
import ru.kozyrev.spring.dto.AnimalDTO;
import ru.kozyrev.spring.dto.search.AnimalSearchDTO;
import ru.kozyrev.spring.dto.search.PageDTO;
import ru.kozyrev.spring.enumerate.AuditCode;
import ru.kozyrev.spring.service.AnimalService;
import ru.kozyrev.spring.util.EntityUtil;
import ru.kozyrev.spring.validator.AnimalDTOValidator;

import java.net.URI;
import java.util.UUID;

/**
 * Контроллер для доступа к животным
 */

@RestController
@Slf4j
public class AnimalControllerImpl implements AnimalController {

    private final AnimalService animalService;

    private final AnimalDTOValidator animalDTOValidator;

    public AnimalControllerImpl(
            final AnimalService animalService,
            final AnimalDTOValidator animalDTOValidator) {
        this.animalService = animalService;
        this.animalDTOValidator = animalDTOValidator;
    }

    @NotNull
    @Override
    public PageDTO<AnimalDTO> getAllAnimals(
            @Nullable final AnimalSearchDTO animalSearchDTO,
            @NotNull final Pageable pageable
    ) {
        log.debug("getAllAnimals with {} - start ", animalSearchDTO);
        final PageDTO<AnimalDTO> result = animalService.getAll(animalSearchDTO, pageable);
        log.debug("getAllAnimals end with result {}", animalSearchDTO);
        return result;
    }

    @Override
    @Nullable
    public AnimalDTO getAnimal(@Nullable final UUID id) {
        log.debug("getAnimal with {} - start ", id);
        final AnimalDTO result = animalService.getOne(id);
        log.debug("getAnimal end with result {}", result);
        return result;

    }

    @Audit(AuditCode.CREATE_ANIMAL)
    @Override
    @NotNull
    public ResponseEntity<AnimalDTO> putAnimal(
            @Nullable final AnimalDTO animalDTO,
            @NotNull final UriComponentsBuilder componentsBuilder,
            @NotNull final BindingResult bindingResult
    ) {
        log.debug("putAnimal with {} - start ", animalDTO);
        EntityUtil.checkDTONotEmpty(animalDTO);
        animalDTOValidator.validate(animalDTO, bindingResult);
        if (bindingResult.hasErrors()) {
            animalDTO.setErrors(bindingResult.getAllErrors());
            return new ResponseEntity<>(animalDTO, HttpStatus.BAD_REQUEST);
        }
        final AnimalDTO addedAnimalDTO = animalService.add(animalDTO);
        final URI uri = componentsBuilder.path("/api/animal/" + addedAnimalDTO.getId())
                .buildAndExpand(addedAnimalDTO)
                .toUri();
        final ResponseEntity<AnimalDTO> result = ResponseEntity.created(uri).body(addedAnimalDTO);
        log.debug("putAnimal end with result {}", result);
        return result;
    }

    @Audit(AuditCode.UPDATE_ANIMAL)
    @Override
    @NotNull
    public ResponseEntity<AnimalDTO> updateAnimal(
            @Nullable final AnimalDTO animalDTO,
            @Nullable final UUID id,
            @NotNull final BindingResult bindingResult
    ) {
        log.debug("updateAnimal with {} - start ", animalDTO);
        EntityUtil.checkDTOEntityUpdate(animalDTO, id);
        animalDTOValidator.validate(animalDTO, bindingResult);
        if (bindingResult.hasErrors()) {
            animalDTO.setErrors(bindingResult.getAllErrors());
            return new ResponseEntity<>(animalDTO, HttpStatus.BAD_REQUEST);
        }
        final AnimalDTO updatedAnimalDTO = animalService.update(animalDTO);
        final ResponseEntity<AnimalDTO> result = new ResponseEntity<>(updatedAnimalDTO, HttpStatus.OK);
        log.debug("updateAnimal end with result {}", result);
        return result;
    }

    @Audit(AuditCode.DELETE_ANIMAL)
    @Override
    public void deleteAnimal(@Nullable final UUID id) {
        log.debug("deleteAnimal with {} - start ", id);
        animalService.delete(id);
        log.debug("deleteAnimal end with {}", id);
    }

    @ModelAttribute
    public AnimalDTO animalDTO() {
        return new AnimalDTO();
    }

    @InitBinder(value = "animalDTO")
    private void initBinder(final WebDataBinder binder) {
        binder.setValidator(animalDTOValidator);
    }
}
