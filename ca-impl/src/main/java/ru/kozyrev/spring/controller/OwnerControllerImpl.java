package ru.kozyrev.spring.controller;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.annotation.Audit;
import ru.kozyrev.spring.api.OwnerController;
import ru.kozyrev.spring.dto.OwnerDTO;
import ru.kozyrev.spring.dto.search.OwnerSearchDTO;
import ru.kozyrev.spring.dto.search.PageDTO;
import ru.kozyrev.spring.enumerate.AuditCode;
import ru.kozyrev.spring.service.OwnerService;
import ru.kozyrev.spring.util.EntityUtil;
import ru.kozyrev.spring.validator.OwnerDTOValidator;

import java.net.URI;
import java.util.UUID;

/**
 * Контроллер для доступа к владельцам
 */
@RestController
@Slf4j
public class OwnerControllerImpl implements OwnerController {

    private final OwnerService ownerService;

    private final OwnerDTOValidator ownerDTOValidator;

    public OwnerControllerImpl(
            final OwnerService ownerService,
            final OwnerDTOValidator ownerDTOValidator
    ) {
        this.ownerService = ownerService;
        this.ownerDTOValidator = ownerDTOValidator;
    }

    @NotNull
    @Override
    public PageDTO<OwnerDTO> getAllOwners(
            @Nullable final OwnerSearchDTO ownerSearchDTO,
            @NotNull final Pageable pageable) {
        log.debug("getAllOwners with {} - start ", ownerSearchDTO);
        final PageDTO<OwnerDTO> result = ownerService.getAll(ownerSearchDTO, pageable);
        log.debug("getAllOwners end with result {}", result);
        return result;
    }

    @Override
    public OwnerDTO getOwner(@Nullable final UUID id) {
        log.debug("getOwner with {} - start ", id);
        final OwnerDTO result = ownerService.getOne(id);
        log.debug("getOwner end with result {}", result);
        return result;
    }

    @Audit(AuditCode.CREATE_OWNER)
    @Override
    @NotNull
    public ResponseEntity<OwnerDTO> putOwner(
            @Nullable final OwnerDTO ownerDTO,
            @NotNull final UriComponentsBuilder componentsBuilder,
            @NotNull final BindingResult bindingResult
    ) {
        log.debug("putOwner with {} - start ", ownerDTO);
        EntityUtil.checkDTONotEmpty(ownerDTO);
        ownerDTOValidator.validate(ownerDTO, bindingResult);
        if (bindingResult.hasErrors()) {
            ownerDTO.setErrors(bindingResult.getAllErrors());
            return new ResponseEntity<>(ownerDTO, HttpStatus.BAD_REQUEST);
        }
        final OwnerDTO addedOwnerDTO = ownerService.add(ownerDTO);
        final URI uri = componentsBuilder.path("/api/owner/" + addedOwnerDTO.getId())
                .buildAndExpand(addedOwnerDTO)
                .toUri();
        final ResponseEntity<OwnerDTO> result = ResponseEntity.created(uri).body(addedOwnerDTO);
        log.debug("putOwner end with result {}", result);
        return result;
    }

    @Audit(AuditCode.UPDATE_OWNER)
    @Override
    @NotNull
    public ResponseEntity<OwnerDTO> updateOwner(
            @Nullable final OwnerDTO ownerDTO,
            @Nullable final UUID id,
            @NotNull final BindingResult bindingResult
    ) {
        log.debug("updateOwner with {} - start ", ownerDTO);
        EntityUtil.checkDTOEntityUpdate(ownerDTO, id);
        ownerDTOValidator.validate(ownerDTO, bindingResult);
        if (bindingResult.hasErrors()) {
            ownerDTO.setErrors(bindingResult.getAllErrors());
            return new ResponseEntity<>(ownerDTO, HttpStatus.BAD_REQUEST);
        }
        final OwnerDTO updatedOwnerDTO = ownerService.update(ownerDTO);
        final ResponseEntity<OwnerDTO> result = new ResponseEntity<>(updatedOwnerDTO, HttpStatus.OK);
        log.debug("updateOwner end with result {}", result);
        return result;
    }

    @Audit(AuditCode.DELETE_OWNER)
    @Override
    public void deleteOwner(@Nullable final UUID id) {
        log.debug("deleteOwner with {} - start ", id);
        ownerService.delete(id);
        log.debug("deleteOwner end with {}", id);
    }

    @ModelAttribute
    public OwnerDTO ownerDTO() {
        return new OwnerDTO();
    }

    @InitBinder(value = "ownerDTO")
    private void initBinder(final WebDataBinder binder) {
        binder.setValidator(ownerDTOValidator);
    }
}
