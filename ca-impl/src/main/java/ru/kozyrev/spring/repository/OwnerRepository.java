package ru.kozyrev.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.kozyrev.spring.entity.Owner;

import java.util.UUID;

/**
 * Репозиторий владельцев
 */
@Repository
public interface OwnerRepository extends JpaRepository<Owner, UUID>, JpaSpecificationExecutor<Owner> {
}
