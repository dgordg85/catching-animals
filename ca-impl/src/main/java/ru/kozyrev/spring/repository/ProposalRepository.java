package ru.kozyrev.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.kozyrev.spring.entity.Proposal;

import java.util.UUID;

/**
 * Репозиторий заявок
 */
@Repository
public interface ProposalRepository extends JpaRepository<Proposal, UUID>, JpaSpecificationExecutor<Proposal> {
}
