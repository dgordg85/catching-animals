package ru.kozyrev.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.kozyrev.spring.entity.Animal;

import java.util.UUID;

/**
 * Репозиторий животных
 */
@Repository
public interface AnimalRepository extends JpaRepository<Animal, UUID>, JpaSpecificationExecutor<Animal> {
}
