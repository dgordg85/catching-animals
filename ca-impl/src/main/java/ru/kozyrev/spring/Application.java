package ru.kozyrev.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"ru.kozyrev.spring.repository"})
@EnableScheduling
@EnableAspectJAutoProxy
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class.getName());

    public static void main(final String[] args) {
        final var application = SpringApplication.run(Application.class, args);
        final BuildProperties buildProperties = application.getBean(BuildProperties.class);
        final ConfigurableEnvironment env = application.getEnvironment();
        final StringBuilder sb = new StringBuilder("\n----------------------------------------------------------\n\t");
        sb.append("Application: ").append(env.getProperty("spring.application.name")).append("\n\t");
        sb.append("Build version is ").append(buildProperties.getVersion()).append("\n\t");
        sb.append("Profile(s): ");
        sb.append(String.join(", ", env.getActiveProfiles()));
        sb.append(" \n").append("----------------------------------------------------------");
        log.info(sb.toString());
    }
}
