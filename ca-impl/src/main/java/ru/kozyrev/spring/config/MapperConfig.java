package ru.kozyrev.spring.config;

import ma.glasnost.orika.MapperFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Configuration;
import ru.kozyrev.spring.dto.AnimalDTO;
import ru.kozyrev.spring.dto.EventDTO;
import ru.kozyrev.spring.dto.OwnerDTO;
import ru.kozyrev.spring.dto.ProposalDTO;
import ru.kozyrev.spring.entity.Animal;
import ru.kozyrev.spring.entity.Event;
import ru.kozyrev.spring.entity.Owner;
import ru.kozyrev.spring.entity.Proposal;

@Configuration
public class MapperConfig {

    MapperFactory mapperFactory;

    public MapperConfig(@NotNull final MapperFactory mapperFactory) {
        this.mapperFactory = mapperFactory;
        mapperFactory
                .classMap(Animal.class, AnimalDTO.class)
                .field("owner.id", "ownerId")
                .field("proposal.id", "proposalId")
                .byDefault()
                .register();
        mapperFactory
                .classMap(Event.class, EventDTO.class)
                .field("animal.id", "animalId")
                .byDefault()
                .register();
        mapperFactory
                .classMap(Owner.class, OwnerDTO.class)
                .byDefault()
                .register();
        mapperFactory
                .classMap(Proposal.class, ProposalDTO.class)
                .byDefault()
                .register();
    }
}
