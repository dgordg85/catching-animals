package ru.kozyrev.spring.enumerate;

import lombok.Getter;
import org.apache.commons.lang.StringUtils;

/**
 * Список полов
 */
@Getter
public enum Gender {
    MALE("Мужской"),
    FEMALE("Женский");

    /**
     * Тип пола
     */
    public String description;

    /**
     * Конструктор создания нового пола
     *
     * @param description тип пола
     */
    Gender(final String description) {
        this.description = description;
    }

    public static Gender getEnum(final String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (final Gender v : values())
            if (v.toString().equals(value)) return v;
        throw new IllegalArgumentException();
    }
}
