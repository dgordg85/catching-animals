package ru.kozyrev.spring.enumerate;

import lombok.Getter;
import org.apache.commons.lang.StringUtils;

/**
 * Список типов животных
 */
@Getter
public enum AnimalType {
    DOG("Собака"),
    CAT("Кот/Кошка");

    /**
     * Название типа животного
     */
    public String description;

    /**
     * Конструктор создания нового типа животного
     *
     * @param description тип животного
     */
    AnimalType(final String description) {
        this.description = description;
    }

    public static AnimalType getEnum(final String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (final AnimalType v : values())
            if (v.toString().equals(value)) return v;
        throw new IllegalArgumentException();
    }
}
