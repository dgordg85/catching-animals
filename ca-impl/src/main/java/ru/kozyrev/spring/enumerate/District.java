package ru.kozyrev.spring.enumerate;

import lombok.Getter;
import org.apache.commons.lang.StringUtils;

/**
 * Список районов
 */
@Getter
public enum District {
    PROSTOKVASHINO("Простоквашино"),
    MORKI("Морки"),
    MARI_TUREK("Мари-Турек");

    /** Название района */
    public String description;

    /**
     * Конструктор создания нового района
     *
     * @param description название района
     */
    District(final String description) {
        this.description = description;
    }

    public static District getEnum(final String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (final District v : values())
            if (v.toString().equals(value)) return v;
        throw new IllegalArgumentException();
    }
}
