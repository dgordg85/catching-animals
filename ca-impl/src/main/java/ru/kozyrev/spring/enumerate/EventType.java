package ru.kozyrev.spring.enumerate;

import lombok.Getter;
import org.apache.commons.lang.StringUtils;

/**
 * Список событий
 */
@Getter
public enum EventType {
    CREATE("Создана"),
    CATCHING("Отлов"),
    TRANSPORTING("Транспортировка"),
    CHIPPING("Чипирование"),
    STERILIZATION("Стерилизация"),
    VACCINATION("Вакцинация"),
    TREATMENT("Лечение"),
    PUTDOWN("Выпуск");

    /**
     * Название события
     */
    public String description;

    /**
     * Конструктор создания нового события
     *
     * @param description название события
     */
    EventType(final String description) {
        this.description = description;
    }

    public static EventType getEnum(final String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (final EventType v : values())
            if (v.toString().equals(value)) return v;
        throw new IllegalArgumentException();
    }
}
