package ru.kozyrev.spring.service;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.kozyrev.spring.dto.TokenDTO;

import java.security.Key;

@Service
public class ValidateService {

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${jwt.secret}")
    private String secret;

    public DefaultClaims validateToken(TokenDTO tokenDTO) {
        Key key = Keys.hmacShaKeyFor(secret.getBytes());

        return (DefaultClaims) Jwts.parserBuilder()
                .setSigningKey(key)
                .requireAudience(applicationName)
                .build()
                .parse(tokenDTO.getToken())
                .getBody();
    }
}
