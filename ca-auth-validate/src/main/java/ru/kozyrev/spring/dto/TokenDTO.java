package ru.kozyrev.spring.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Модель JWT токена")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TokenDTO {

    @ApiModelProperty(required = true, value = "JWT токен", example = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwiYXVkIjoiY2EtYXV0aCIsInJvbGUiOiJVU0VSIiwiaXNzIjoiY2EtYXV0aCIsImV4cCI6MTYxMzIyODgyMCwiaWF0IjoxNjEzMjI1MjIwLCJqdGkiOiJjOWFlZWE1OC1hZmVlLTRlZjAtOGViOC1kYmQ3NWQxY2FjNTkiLCJlbWFpbCI6IjU1QDIucnUifQ.ycPnkyBQ0P39fg52Vpogk2svUxiMJMQ_76rW9gSilY2livT6kbuT5T92Lb6VXrwd")
    private String token;
}
