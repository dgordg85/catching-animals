package ru.kozyrev.spring.enumerate;

/**
 * Статус лога
 */
public enum LogEventStatus {

    /**
     * Лог начат
     */
    START,

    /**
     * Лог завершен успешно
     */
    SUCCESS,

    /**
     * Лог завершен неуспешно
     */
    FAILURE
}
