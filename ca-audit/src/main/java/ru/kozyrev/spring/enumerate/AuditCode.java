package ru.kozyrev.spring.enumerate;

/**
 * Перечисления для методов аудита
 */
public enum AuditCode {
    /**
     * Создание животного
     */
    CREATE_ANIMAL,

    /**
     * Обновление животного
     */
    UPDATE_ANIMAL,

    /**
     * Удаление животного
     */
    DELETE_ANIMAL,

    /**
     * Создание события
     */
    CREATE_EVENT,

    /**
     * Обновление события
     */
    UPDATE_EVENT,

    /**
     * Удаление события
     */
    DELETE_EVENT,

    /**
     * Создание владельца
     */
    CREATE_OWNER,

    /**
     * Обновление владельца
     */
    UPDATE_OWNER,

    /**
     * Удаление владельца
     */
    DELETE_OWNER,

    /**
     * Создание заявки
     */
    CREATE_PROPOSAL,

    /**
     * Обновление заявки
     */
    UPDATE_PROPOSAL,

    /**
     * Удаление заявки
     */
    DELETE_PROPOSAL,

    /**
     * Создание пользователя
     */
    CREATE_USER,

    /**
     * Обновление пользователя
     */
    UPDATE_USER,

    /**
     * Удаление пользователя
     */
    DELETE_USER,

    /**
     * Создание нового токена
     */
    CREATE_TOKEN,

    /**
     * Валидация токена
     */
    VALIDATE_TOKEN
}
