package ru.kozyrev.spring.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.annotation.Audit;
import ru.kozyrev.spring.entity.EventLogEntity;
import ru.kozyrev.spring.enumerate.LogEventStatus;

import java.time.Instant;
import java.util.Arrays;

/**
 * Аспект для аудита события добавления, обновления, удаления
 */
@Slf4j
@Component
@Aspect
public class AspectService {

    /**
     * Срез события публичного метода с аннотацией Audit
     */
    @Pointcut("@annotation(ru.kozyrev.spring.annotation.Audit) && execution(public * *(..))")
    public void publicAspectAudit() {
    }

    @Around(value = "publicAspectAudit()")
    public Object auditStart(final ProceedingJoinPoint joinPoint) throws Throwable {
        final EventLogEntity eventLogEntity = new EventLogEntity();
        auditBegin(joinPoint, eventLogEntity);
        final Object proceed;
        try {
            proceed = joinPoint.proceed();
        } catch (final Exception e) {
            eventLogEntity.setLogEventStatus(LogEventStatus.FAILURE);
            eventLogEntity.setTimeEnd(Instant.now());
            log.info(eventLogEntity.toString());
            throw e;
        }
        auditEnd(proceed, eventLogEntity);
        return proceed;
    }

    private void auditBegin(final ProceedingJoinPoint jp, final EventLogEntity eventLogEntity) throws JsonProcessingException {
        final MethodSignature methodSignature = (MethodSignature) jp.getSignature();
        final Audit annotation = methodSignature.getMethod().getAnnotation(Audit.class);
        eventLogEntity.setAuditCode(annotation.value());
        eventLogEntity.setLogEventStatus(LogEventStatus.START);
        eventLogEntity.setTimeStart(Instant.now());
        final Object[] params = Arrays.stream(jp.getArgs())
                .filter(obj -> !(obj instanceof BindingResult))
                .filter(obj -> !(obj instanceof UriComponentsBuilder))
                .toArray();
        try {
            eventLogEntity.setParams(new ObjectMapper().writeValueAsString(params));
        } catch (final JsonProcessingException e) {
            log.info(eventLogEntity.toString());
            eventLogEntity.setLogEventStatus(LogEventStatus.FAILURE);
            eventLogEntity.setTimeEnd(Instant.now());
            log.info(eventLogEntity.toString());
            throw e;
        }
        log.info(eventLogEntity.toString());
    }

    private void auditEnd(final Object obj, final EventLogEntity eventLogEntity) {
        try {
            eventLogEntity.setReturnValue(new ObjectMapper().writeValueAsString(obj));
        } catch (final JsonProcessingException e) {
            e.printStackTrace();
        }
        eventLogEntity.setLogEventStatus(LogEventStatus.SUCCESS);
        eventLogEntity.setTimeEnd(Instant.now());
        log.info(eventLogEntity.toString());
    }
}
