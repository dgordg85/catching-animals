package ru.kozyrev.spring.entity;

import lombok.Getter;
import lombok.Setter;
import ru.kozyrev.spring.enumerate.AuditCode;
import ru.kozyrev.spring.enumerate.LogEventStatus;

import java.time.Instant;
import java.util.UUID;

/**
 * Класс сущности события аудита
 */
@Getter
@Setter
public class EventLogEntity {

    /**
     * Уникальный идентификатор для базы данных, автоинкремент
     */
    private int id;

    /** Индентификатор события */
    private UUID uuid = UUID.randomUUID();

    /** Enum код, уникальный для кадого метода */
    private AuditCode auditCode;

    /** Enum код статусов события: START, SUCCESS, FAILURE */
    private LogEventStatus logEventStatus;

    /** Время начала события */
    private Instant timeStart;

    /** Время окончания события */
    private Instant timeEnd;

    /** Имя пользователя */
    private String username = "";

    /** Входящие параметры */
    private String params;

    /** Исходящий результат */
    private String returnValue;

    @Override
    public String toString() {
        return String.format("%s;%s;%s;%s;%s;%s;%s;%s",
                uuid, auditCode, logEventStatus, timeStart,
                timeEnd, username, params, returnValue
        );
    }
}
