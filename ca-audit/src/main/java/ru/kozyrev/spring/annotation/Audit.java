package ru.kozyrev.spring.annotation;

import ru.kozyrev.spring.enumerate.AuditCode;
import ru.kozyrev.spring.service.AspectService;

import java.lang.annotation.*;

/**
 * Аннотация для аудита методов
 *
 * @see AspectService
 * @see AuditCode
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Audit {

    AuditCode value();
}
