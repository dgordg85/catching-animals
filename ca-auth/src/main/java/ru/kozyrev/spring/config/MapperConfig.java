package ru.kozyrev.spring.config;

import ma.glasnost.orika.MapperFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Configuration;
import ru.kozyrev.spring.dto.UserDTO;
import ru.kozyrev.spring.entity.User;


@Configuration
public class MapperConfig {

    MapperFactory mapperFactory;

    public MapperConfig(@NotNull final MapperFactory mapperFactory) {
        this.mapperFactory = mapperFactory;
        mapperFactory
                .classMap(User.class, UserDTO.class)
                .byDefault()
                .register();
    }
}
