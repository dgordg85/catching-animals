package ru.kozyrev.spring.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "swagger")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SwaggerProperties {

    private String title;

    private String description;

    private Contact contact = new Contact();

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Contact {

        private String mail;

        private String name;

        private String url;
    }
}
