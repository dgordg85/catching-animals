package ru.kozyrev.spring.service;

import ma.glasnost.orika.MapperFacade;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kozyrev.spring.dto.UserDTO;
import ru.kozyrev.spring.entity.User;
import ru.kozyrev.spring.enumerate.Status;
import ru.kozyrev.spring.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
public class UserService {

    @NotNull
    private final MapperFacade mapperFacade;

    @NotNull
    private final UserRepository userRepository;

    public UserService(@NotNull final MapperFacade mapperFacade, @NotNull final UserRepository userRepository) {
        this.mapperFacade = mapperFacade;
        this.userRepository = userRepository;
    }

    @NotNull
    @Transactional
    public UserDTO add(@NotNull final UserDTO userDTO) {
        final User user = mapperFacade.map(userDTO, User.class);
        final User result = userRepository.save(user);
        return mapperFacade.map(result, UserDTO.class);
    }

    @NotNull
    @Transactional(readOnly = true)
    public UserDTO get(@NotNull final UUID id) {
        final User user = userRepository.getOne(id);
        if (!user.getEnabled()) {
            throw new EntityNotFoundException();
        }
        return mapperFacade.map(user, UserDTO.class);

    }

    @NotNull
    @Transactional
    public UserDTO update(@NotNull final UserDTO userDTO) {
        final User user = mapperFacade.map(userDTO, User.class);
        final User result = userRepository.save(user);
        return mapperFacade.map(result, UserDTO.class);
    }

    @Transactional
    public void delete(@NotNull final UUID id) {
        final User user = userRepository.getOne(id);
        user.setStatus(Status.DELETED);
        user.setEnabled(false);
        userRepository.save(user);
    }
}
