package ru.kozyrev.spring.service;

import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.kozyrev.spring.dto.AuthUserDTO;
import ru.kozyrev.spring.dto.TokenDTO;
import ru.kozyrev.spring.exception.AuthentificationNotFoundException;
import ru.kozyrev.spring.repository.UserRepository;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j
public class AuthService {

    UserRepository userRepository;

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.lifetime}")
    private int lifeTime;

    @Value("${spring.application.name}")
    private String issuer;

    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public TokenDTO createToken(AuthUserDTO authUserDTO) {
        var user = userRepository.findUserByUsername(authUserDTO.getLogin())
                .orElseThrow(AuthentificationNotFoundException::new);
        if (!user.getPassword().equals(authUserDTO.getPassword())) {
            throw new AuthentificationNotFoundException();
        }

        Key key = Keys.hmacShaKeyFor(secret.getBytes());
        Map<String, Object> map = new HashMap<>();
        map.put("email", user.getEmail());
        map.put("role", user.getRole());

        var jwt = Jwts.builder().setHeaderParam(Header.TYPE, Header.JWT_TYPE)
                .setSubject(user.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + lifeTime))
                .setAudience(authUserDTO.getAudience())
                .setId(UUID.randomUUID().toString())
                .setIssuer(issuer)
                .addClaims(map)
                .signWith(key, SignatureAlgorithm.HS256).compact();
        return new TokenDTO(jwt);
    }
}
