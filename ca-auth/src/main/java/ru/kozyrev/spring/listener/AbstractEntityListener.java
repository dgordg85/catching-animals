package ru.kozyrev.spring.listener;

import ru.kozyrev.spring.entity.User;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.Instant;

public class AbstractEntityListener {

    @PrePersist
    private void setCreateAt(final User user) {
        user.setCreatedAt(Instant.now());
        user.setUpdatedAt(user.getCreatedAt());
    }

    @PreUpdate
    private void setUpdateAt(final User user) {
        user.setUpdatedAt(Instant.now());
    }

}
