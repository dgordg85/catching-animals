package ru.kozyrev.spring.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.spring.enumerate.Role;
import ru.kozyrev.spring.enumerate.Status;
import ru.kozyrev.spring.listener.AbstractEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "users", schema = "ca_auth")
@EntityListeners(AbstractEntityListener.class)
public class User implements Serializable {

    /**
     * Уникальный идентификатор пользователя
     */
    @Nullable
    @Id
    @GeneratedValue
    private UUID id;

    /**
     * Логни пользователя - уникальный
     */
    @NotNull
    @Column(nullable = false)
    private String username;

    /**
     * Пароль пользователя
     */
    @NotNull
    @Column(nullable = false)
    private String password;

    /**
     * Роль пользователя в системе
     */
    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    /**
     * Электронный почтовый ящик
     */
    @NotNull
    @Column(nullable = false)
    private String email;

    /**
     * Статус пользователя
     */
    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    /**
     * Признак активности пользователя
     */
    @NotNull
    @Column(nullable = false)
    private Boolean enabled = true;

    /**
     * Время создания пользователя
     */
    @NotNull
    @Column(nullable = false, updatable = false)
    private Instant createdAt;

    /**
     * Время обновления пользователя
     */
    @NotNull
    @Column(nullable = false)
    private Instant updatedAt;
}
