package ru.kozyrev.spring.controller;


import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.annotation.Audit;
import ru.kozyrev.spring.api.UserController;
import ru.kozyrev.spring.dto.UserDTO;
import ru.kozyrev.spring.enumerate.AuditCode;
import ru.kozyrev.spring.exception.EmptyException;
import ru.kozyrev.spring.exception.UpdateFailException;
import ru.kozyrev.spring.service.UserService;

import java.net.URI;
import java.util.UUID;

/**
 * Контроллер доступа к пользователю
 */
@RestController
@Slf4j
public class UserControllerImpl implements UserController {

    private final UserService userService;

    public UserControllerImpl(final UserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    @Audit(AuditCode.CREATE_USER)
    public ResponseEntity<UserDTO> createUser(
            @Nullable final UserDTO userDTO,
            @NotNull final UriComponentsBuilder componentsBuilder
    ) {
        log.debug("createUser with {} - start ", userDTO);
        if (userDTO == null) {
            throw new EmptyException();
        }
        final UserDTO result = userService.add(userDTO);
        final URI uri = componentsBuilder.path("/api/user/" + result.getId()).buildAndExpand(result).toUri();
        log.debug("createUser end with result {}", result);
        return ResponseEntity.created(uri).body(result);
    }

    @Nullable
    @Override
    public ResponseEntity<UserDTO> getUser(@Nullable final UUID id) {
        log.debug("getUser with {} - start ", id);
        if (id == null) {
            throw new EmptyException();
        }
        final UserDTO result = userService.get(id);
        log.debug("getUser end with result {}", result);
        return ResponseEntity.ok().body(result);
    }

    @NotNull
    @Override
    @Audit(AuditCode.UPDATE_USER)

    public ResponseEntity<UserDTO> updateUser(@Nullable final UserDTO userDTO, @Nullable final UUID id) {
        log.debug("updateUser with {} - start ", userDTO);
        if (id == null || userDTO == null) {
            throw new EmptyException();
        }
        if (userDTO.getId() == null || userDTO.getId().compareTo(id) != 0) {
            throw new UpdateFailException();
        }
        final UserDTO result = userService.update(userDTO);
        log.debug("updateUser end with result {}", result);
        return ResponseEntity.ok().body(result);
    }

    @Override
    @Audit(AuditCode.DELETE_USER)
    public void deleteUser(@Nullable final UUID id) {
        log.debug("deleteUser with {} - start ", id);
        if (id == null) {
            throw new EmptyException();
        }
        userService.delete(id);
        log.debug("deleteUser end with result {}", id);
    }
}
