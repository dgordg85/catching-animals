package ru.kozyrev.spring.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.kozyrev.spring.annotation.Audit;
import ru.kozyrev.spring.api.AuthController;
import ru.kozyrev.spring.dto.AuthUserDTO;
import ru.kozyrev.spring.dto.TokenDTO;
import ru.kozyrev.spring.enumerate.AuditCode;
import ru.kozyrev.spring.service.AuthService;

@RestController
@AllArgsConstructor
@Slf4j
public class AuthControllerImpl implements AuthController {

    AuthService authService;

    @NotNull
    @Override
    @Audit(AuditCode.CREATE_TOKEN)
    public ResponseEntity<TokenDTO> getToken(AuthUserDTO authUserDTO) {
        log.debug("getToken with {} - start ", authUserDTO);
        var result = authService.createToken(authUserDTO);
        log.debug("createUser end with result {}", result);
        return ResponseEntity.ok(result);
    }
}
