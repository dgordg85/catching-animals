package ru.kozyrev.spring.exception;

public class AuthentificationNotFoundException extends RuntimeException {

    public AuthentificationNotFoundException() {
        super("Не найден пользователь с таким логином или паролем");
    }
}
