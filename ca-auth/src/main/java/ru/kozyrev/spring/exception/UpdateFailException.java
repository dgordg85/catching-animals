package ru.kozyrev.spring.exception;

/**
 * исключение выбрасывается, если сущность для обновления не найдена в базе по переданному UUID
 * или запрос не совпадает с переданной сущностью
 */
public class UpdateFailException extends RuntimeException {

    public UpdateFailException() {
        super("Элемент для обновления не найден!");
    }
}
