package ru.kozyrev.spring.api;

import io.swagger.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.dto.ProposalDTO;
import ru.kozyrev.spring.dto.search.PageDTO;
import ru.kozyrev.spring.dto.search.ProposalSearchDTO;

import java.util.UUID;

/**
 * Интерфейс контроллера заявок
 */
@Api(value = "API получения заявок")
@RequestMapping("/api/proposal")
public interface ProposalController {

    /**
     * Метод получения заявки
     *
     * @param id идентификатор заявки
     * @return DTO объект заявки
     */
    @Nullable
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Получить заявки", notes = "Позволяет получить заявка по UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Заявка успешно получена"),
            @ApiResponse(code = 404, message = "Заявка не найдена")
    })
    ProposalDTO getProposal(@ApiParam(value = "Идентификатор заявки", required = true, example = "fa3d926a-6ea0-4716-9f87-af62edf5c852")
                            @Nullable @PathVariable UUID id);

    /**
     * Метод добавления заявки
     *
     * @param proposalDTO       объект DTO заявки
     * @param componentsBuilder для формирования URI объекта
     * @param bindingResult     интерфейс для регистрации ошибок
     * @return ответ с объектом, ошибками, статусом
     */
    @NotNull
    @PostMapping
    @ApiOperation(value = "Добавить заявку", notes = "Позволяет добавить заявку через передачу DTO объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Заявка успешно добавлена"),
            @ApiResponse(code = 400, message = "Ошибка заполнения полей")
    })
    ResponseEntity<ProposalDTO> putProposal(
            @ApiParam(required = true, value = "DTO объект заявки")
            @Nullable @RequestBody ProposalDTO proposalDTO,
            @NotNull UriComponentsBuilder componentsBuilder,
            @NotNull BindingResult bindingResult
    );

    /**
     * Метод обновления заявки
     *
     * @param proposalDTO     объект DTO заявки
     * @param id            идентификатор заявки
     * @param bindingResult интерфейс для регистрации ошибок
     * @return ответ с объектом, ошибками, статусом
     */
    @NotNull
    @PutMapping("/{id}")
    @ApiOperation(value = "Обновить заявку", notes = "Позволяет обновить заявку через передачу DTO объекта, " +
            "UUID передаваемой заявки и параметр id должны совпадать")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Заявка успешно обновлена"),
            @ApiResponse(code = 400, message = "Ошибка заполнения полей"),
            @ApiResponse(code = 404, message = "Заявка для обновления не найдена")
    })
    ResponseEntity<ProposalDTO> updateProposal(
            @ApiParam(required = true, value = "DTO объект заявки")
            @Nullable @RequestBody ProposalDTO proposalDTO,
            @ApiParam(value = "Идентификатор заявки", required = true, example = "fa3d926a-6ea0-4716-9f87-af62edf5c852")
            @Nullable @PathVariable UUID id,
            @NotNull BindingResult bindingResult
    );

    /**
     * Метод для удаления заявки
     *
     * @param id идентификатор заявки
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Удалить заявку", notes = "Позволяет удалить заявку по UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Заявка успешно удалена"),
            @ApiResponse(code = 404, message = "Заявка не найдена")
    })
    void deleteProposal(@ApiParam(value = "Идентификатор заявки", required = true, example = "fa3d926a-6ea0-4716-9f87-af62edf5c852")
                        @PathVariable @Nullable UUID id);

    /**
     * Метод позволяет получить Заявки постранично
     *
     * @param proposalSearchDTO объект для поиска, включающий поля поиска
     * @param pageable          объект содержащий пагинацию
     * @return возвращается List объектов и информацию о страницах
     */
    @NotNull
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Получить заявку", notes = "Позволяет получить все заявки постранично, " +
            "с указанным количеством объектов на страницу, нужную страницу, с сортировкой")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "pageSize", value = "Размер страницы", example = "5", defaultValue = "5"),
            @ApiImplicitParam(name = "pageNumber", value = "Номер страницы", example = "1", defaultValue = "0")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Страница заявок успешна получена")
    })
    PageDTO<ProposalDTO> getAllProposals(@ApiParam(required = true, value = "Поисковый DTO объект заявки") @RequestBody @Nullable ProposalSearchDTO proposalSearchDTO, @NotNull Pageable pageable);
}
