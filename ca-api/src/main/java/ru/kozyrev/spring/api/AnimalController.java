package ru.kozyrev.spring.api;

import io.swagger.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.dto.AnimalDTO;
import ru.kozyrev.spring.dto.search.AnimalSearchDTO;
import ru.kozyrev.spring.dto.search.PageDTO;

import java.util.UUID;

/**
 * Интерфейс контроллера животных
 */
@Api(value = "API получения животных")
@RequestMapping("/api/animal")
public interface AnimalController {

    /**
     * Метод получения животного
     *
     * @param id идентификатор животного
     * @return DTO объект животного
     */
    @Nullable
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Получить животное", notes = "Позволяет получить животное по UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Животное успешно получено"),
            @ApiResponse(code = 404, message = "Животное не найдено")
    })
    AnimalDTO getAnimal(@ApiParam(value = "Идентификатор животного", required = true, example = "da6f68fd-3395-435b-8624-be575b943574")
                        @Nullable @PathVariable UUID id);

    /**
     * Метод добавления животного
     *
     * @param animalDTO         объект DTO животного
     * @param componentsBuilder для формирования URI объекта
     * @param bindingResult     интерфейс для регистрации ошибок
     * @return ответ с объектом, ошибками, статусом
     */
    @NotNull
    @PostMapping
    @ApiOperation(value = "Добавить животное", notes = "Позволяет добавить животное через передачу DTO объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Животное успешно добавлено"),
            @ApiResponse(code = 400, message = "Ошибка заполнения полей")
    })
    ResponseEntity<AnimalDTO> putAnimal(
            @ApiParam(required = true, value = "DTO объект животного")
            @Nullable @RequestBody AnimalDTO animalDTO,
            @NotNull UriComponentsBuilder componentsBuilder,
            @NotNull BindingResult bindingResult
    );

    /**
     * Метод обновления животного
     *
     * @param animalDTO     объект DTO животного
     * @param id            идентификатор животного
     * @param bindingResult интерфейс для регистрации ошибок
     * @return ответ с объектом, ошибками, статусом
     */
    @NotNull
    @PutMapping("/{id}")
    @ApiOperation(value = "Обновить животное", notes = "Позволяет обновить животное через передачу DTO объекта, " +
            "UUID передаваемого животного и параметр id должны совпадать")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Животное успешно обновлено"),
            @ApiResponse(code = 400, message = "Ошибка заполнения полей"),
            @ApiResponse(code = 404, message = "Животное для обновления не найдено")
    })
    ResponseEntity<AnimalDTO> updateAnimal(
            @ApiParam(required = true, value = "DTO объект животного")
            @Nullable @RequestBody AnimalDTO animalDTO,
            @ApiParam(value = "Идентификатор животного", required = true, example = "da6f68fd-3395-435b-8624-be575b943574")
            @Nullable @PathVariable UUID id,
            @NotNull BindingResult bindingResult
    );

    /**
     * Метод для удаления животного
     *
     * @param id идентификатор животного
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Удалить животное", notes = "Позволяет удалить животное по UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Животное успешно удалено"),
            @ApiResponse(code = 404, message = "Животное не найдено")
    })
    void deleteAnimal(@ApiParam(value = "Идентификатор животного", required = true, example = "da6f68fd-3395-435b-8624-be575b943574")
                      @PathVariable @Nullable UUID id);

    /**
     * Метод позволяет получить Животных постранично
     *
     * @param animalSearchDTO объект для поиска, включающий поля поиска
     * @param pageable        объект содержащий пагинацию
     * @return возвращается List объектов и информацию о страницах
     */
    @NotNull
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Получить животных", notes = "Позволяет получить всех животных постранично, " +
            "с указанным количеством объектов на страницу, нужную страницу, с сортировкой")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "pageSize", value = "Размер страницы", example = "5", defaultValue = "5"),
            @ApiImplicitParam(name = "pageNumber", value = "Номер страницы", example = "1", defaultValue = "0")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Страница животных успешна получена")
    })
    PageDTO<AnimalDTO> getAllAnimals(@ApiParam(required = true, value = "Поисковый DTO объект животного") @RequestBody @Nullable AnimalSearchDTO animalSearchDTO, @NotNull Pageable pageable);
}
