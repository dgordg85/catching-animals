package ru.kozyrev.spring.api;

import io.swagger.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.dto.EventDTO;
import ru.kozyrev.spring.dto.search.EventSearchDTO;
import ru.kozyrev.spring.dto.search.PageDTO;

import java.util.UUID;

/**
 * Интерфейс контроллера событий
 */
@Api(value = "API получения событий")
@RequestMapping("/api/event")
public interface EventController {

    /**
     * Метод получения события
     *
     * @param id идентификатор события
     * @return DTO объект события
     */
    @Nullable
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Получить событие", notes = "Позволяет получить событие по UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Событие успешно получено"),
            @ApiResponse(code = 404, message = "Событие не найдено")
    })
    EventDTO getEvent(@ApiParam(value = "Идентификатор события", required = true, example = "4f99086c-685b-4e22-ac43-0efe2dfaa234")
                      @Nullable
                      @PathVariable UUID id);

    /**
     * Метод добавления события
     *
     * @param eventDTO          объект DTO события
     * @param componentsBuilder для формирования URI объекта
     * @param bindingResult     интерфейс для регистрации ошибок
     * @return ответ с объектом, ошибками, статусом
     */
    @NotNull
    @PostMapping
    @ApiOperation(value = "Добавить событие", notes = "Позволяет добавить событие через передачу DTO объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Событие успешно добавлено"),
            @ApiResponse(code = 400, message = "Ошибка заполнения полей")
    })
    ResponseEntity<EventDTO> putEvent(
            @ApiParam(required = true, value = "DTO объект события") @Nullable @RequestBody EventDTO eventDTO,
            @NotNull UriComponentsBuilder componentsBuilder,
            @NotNull BindingResult bindingResult
    );

    /**
     * Метод обновления события
     *
     * @param eventDTO      объект DTO события
     * @param id            идентификатор события
     * @param bindingResult интерфейс для регистрации ошибок
     * @return ответ с объектом, ошибками, статусом
     */
    @NotNull
    @PutMapping("/{id}")
    @ApiOperation(value = "Обновить событие", notes = "Позволяет обновить событие через передачу DTO объекта, " +
            "UUID передаваемого события и параметр id должны совпадать")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Событие успешно обновлено"),
            @ApiResponse(code = 400, message = "Ошибка заполнения полей"),
            @ApiResponse(code = 404, message = "Событие для обновления не найдено")
    })
    ResponseEntity<EventDTO> updateEvent(
            @ApiParam(required = true, value = "DTO объект события")
            @Nullable @RequestBody EventDTO eventDTO,
            @ApiParam(value = "Идентификатор события", required = true, example = "4f99086c-685b-4e22-ac43-0efe2dfaa234")
            @Nullable @PathVariable UUID id,
            @NotNull BindingResult bindingResult
    );

    /**
     * Метод для удаления события
     *
     * @param id идентификатор события
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Удалить событие", notes = "Позволяет удалить событие по UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Событие успешно удалено"),
            @ApiResponse(code = 404, message = "Событие не найдено")
    })
    void deleteEvent(@ApiParam(value = "Идентификатор события", required = true, example = "4f99086c-685b-4e22-ac43-0efe2dfaa234")
                     @PathVariable @Nullable UUID id);

    /**
     * Метод позволяет получить События постранично
     *
     * @param eventSearchDTO объект для поиска, включающий поля поиска
     * @param pageable       объект содержащий пагинацию
     * @return возвращается List объектов и информацию о страницах
     */
    @NotNull
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Получить события", notes = "Позволяет получить все события постранично, " +
            "с указанным количеством объектов на страницу, нужную страницу, с сортировкой")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "pageSize", value = "Размер страницы", example = "5", defaultValue = "5"),
            @ApiImplicitParam(name = "pageNumber", value = "Номер страницы", example = "1", defaultValue = "0")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Страница событий успешна получена")
    })
    PageDTO<EventDTO> getAllEvents(@ApiParam(required = true, value = "Поисковый DTO объект события") @RequestBody @Nullable EventSearchDTO eventSearchDTO, @NotNull Pageable pageable);
}
