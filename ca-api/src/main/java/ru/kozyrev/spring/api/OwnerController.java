package ru.kozyrev.spring.api;

import io.swagger.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kozyrev.spring.dto.OwnerDTO;
import ru.kozyrev.spring.dto.search.OwnerSearchDTO;
import ru.kozyrev.spring.dto.search.PageDTO;

import java.util.UUID;

/**
 * Интерфейс контроллера владельцев
 */
@Api(value = "API получения владельцев")
@RequestMapping("/api/owner")
public interface OwnerController {

    /**
     * Метод получения владельца
     *
     * @param id идентификатор владельца
     * @return DTO объект владельца
     */
    @Nullable
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Получить владельца", notes = "Позволяет получить владельца по UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Владелец успешно получен"),
            @ApiResponse(code = 404, message = "Владелец не найден")
    })
    OwnerDTO getOwner(@ApiParam(value = "Идентификатор владельца", required = true, example = "03e955f9-56ef-494d-8caa-e035f4018520")
                      @Nullable @PathVariable UUID id);

    /**
     * Метод добавления владельца
     *
     * @param ownerDTO          объект DTO владельца
     * @param componentsBuilder для формирования URI объекта
     * @param bindingResult     интерфейс для регистрации ошибок
     * @return ответ с объектом, ошибками, статусом
     */
    @NotNull
    @PostMapping
    @ApiOperation(value = "Добавить владельца", notes = "Позволяет добавить владельца через передачу DTO объекта")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Владелец успешно добавлен"),
            @ApiResponse(code = 400, message = "Ошибка заполнения полей")
    })
    ResponseEntity<OwnerDTO> putOwner(
            @ApiParam(required = true, value = "DTO объект владельца")
            @Nullable @RequestBody OwnerDTO ownerDTO,
            @NotNull UriComponentsBuilder componentsBuilder,
            @NotNull BindingResult bindingResult
    );

    /**
     * Метод обновления владельца
     *
     * @param ownerDTO     объект DTO владельца
     * @param id            идентификатор владельца
     * @param bindingResult интерфейс для регистрации ошибок
     * @return ответ с объектом, ошибками, статусом
     */
    @NotNull
    @PutMapping("/{id}")
    @ApiOperation(value = "Обновить владельца", notes = "Позволяет обновить владельца через передачу DTO объекта, " +
            "UUID передаваемого владельца и параметр id должны совпадать")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Владелец успешно обновлен"),
            @ApiResponse(code = 400, message = "Ошибка заполнения полей"),
            @ApiResponse(code = 404, message = "Владелец для обновления не найден")
    })
    ResponseEntity<OwnerDTO> updateOwner(
            @ApiParam(required = true, value = "DTO объект владельца")
            @Nullable @RequestBody OwnerDTO ownerDTO,
            @ApiParam(value = "Идентификатор владельца", required = true, example = "03e955f9-56ef-494d-8caa-e035f4018520")
            @Nullable @PathVariable UUID id,
            @NotNull BindingResult bindingResult
    );

    /**
     * Метод для удаления владельца
     *
     * @param id идентификатор владельца
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Удалить владельца", notes = "Позволяет удалить владельца по UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Владелец успешно удален"),
            @ApiResponse(code = 404, message = "Владелец не найден")
    })
    void deleteOwner(@ApiParam(value = "Идентификатор владельца", required = true, example = "03e955f9-56ef-494d-8caa-e035f4018520")
                     @PathVariable @Nullable UUID id);

    /**
     * Метод позволяет получить Владельцев постранично
     *
     * @param ownerSearchDTO объект для поиска, включающий поля поиска
     * @param pageable       объект содержащий пагинацию
     * @return возвращается List объектов и информацию о страницах
     */
    @NotNull
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Получить владельцев", notes = "Позволяет получить всех владельцев постранично, " +
            "с указанным количеством объектов на страницу, нужную страницу, с сортировкой")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "pageSize", value = "Размер страницы", example = "5", defaultValue = "5"),
            @ApiImplicitParam(name = "pageNumber", value = "Номер страницы", example = "1", defaultValue = "0")
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Страница владельцев успешна получена")
    })
    PageDTO<OwnerDTO> getAllOwners(@ApiParam(required = true, value = "Поисковый DTO объект владельца") @RequestBody @Nullable OwnerSearchDTO ownerSearchDTO, @NotNull Pageable pageable);
}
