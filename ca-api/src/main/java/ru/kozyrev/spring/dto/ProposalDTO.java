package ru.kozyrev.spring.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

/**
 * Класс DTO заявок
 */
@Getter
@Setter
@ApiModel(description = "Модель DTO заявки")
public class ProposalDTO extends AbstractDTO {

    /**
     * Поле название заявки
     */
    @NotNull
    @ApiModelProperty(value = "Название заявки", required = true, example = "Выезд в Простоквашино")
    private String name;

    /**
     * Поле описание заявки
     */
    @NotNull
    @ApiModelProperty(value = "Описание заявки", required = true, example = "Необходимо отловить беспризорных животных, количество двое")
    private String description;

    /**
     * Поле района (области) заявки
     */
    @NotNull
    @ApiModelProperty(value = "Район заявки", required = true, example = "MORKI")
    private String district;

    /**
     * Поле является ли заявка контрактом
     */
    @NotNull
    @ApiModelProperty(value = "Являетлся ли заявка контрактом", required = true, example = "true")
    private Boolean isContract = false;
}
