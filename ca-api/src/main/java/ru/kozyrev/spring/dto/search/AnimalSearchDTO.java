package ru.kozyrev.spring.dto.search;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.spring.util.LocalDateDeserializer;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Класс параметров для поиска Животного
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Объект для поиска животного")
public class AnimalSearchDTO {

    /**
     * Поле кличка
     */
    @Nullable
    @ApiModelProperty(value = "Имя животного", example = "Шарик")
    private String name;

    /**
     * Поле пол
     */
    @Nullable
    @ApiModelProperty(value = "Пол животного", allowableValues = "MALE, FEMALE", example = "MALE")
    private String gender;

    /**
     * Поле описание животного
     */
    @Nullable
    @ApiModelProperty(value = "Описание животного", example = "Любит фотоохоту")
    private String description;

    /**
     * Поле типа животного
     */
    @Nullable
    @ApiModelProperty(value = "Тип животного", allowableValues = "DOG, CAT", example = "DOG")
    private String type;

    /**
     * Поле владелец животного
     */
    @Nullable
    @ApiModelProperty(value = "ID владельца животного", example = "03e955f9-56ef-494d-8caa-e035f4018520")
    private UUID ownerId;

    /**
     * Поле принадлежности к заявке
     */
    @Nullable
    @ApiModelProperty(value = "ID заявки", example = "fa3d926a-6ea0-4716-9f87-af62edf5c852")
    private UUID proposalId;

    /**
     * Поле день рождение после
     */
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @Nullable
    @ApiModelProperty(value = "Дата фильтрации дня рождения От", example = "2016-01-01")
    private LocalDate birthdayAfter;

    /**
     * Поле день рождение до
     */
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @Nullable
    @ApiModelProperty(value = "Дата фильтрации дня рождения До", example = "2018-01-01")
    private LocalDate birthdayBefore;
}
