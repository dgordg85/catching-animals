package ru.kozyrev.spring.dto.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

/**
 * Класс параметров для поиска Владельца
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Объект для поиска заявок")
public class ProposalSearchDTO {

    /**
     * Поле название заявки
     */
    @Nullable
    @ApiModelProperty(value = "Название заявки", example = "Выезд в Простоквашино")
    private String name;

    /**
     * Поле описание заявки
     */
    @Nullable
    @ApiModelProperty(value = "Описание заявки", example = "Необходимо отловить беспризорных животных, количество двое")
    private String description;

    /**
     * Поле района (области) заявки
     */
    @Nullable
    @ApiModelProperty(value = "Район заявки", example = "MORKI")
    private String district;

    /**
     * Поле является ли заявка контрактом
     */
    @Nullable
    @ApiModelProperty(value = "Является ли заявка контрактом", example = "true")
    private Boolean isContract = false;
}
