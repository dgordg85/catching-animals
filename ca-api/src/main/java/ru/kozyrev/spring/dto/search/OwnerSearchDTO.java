package ru.kozyrev.spring.dto.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

/**
 * Класс параметров для поиска Владельца
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Объект для поиска владельцев")
public class OwnerSearchDTO {

    /**
     * Имя владельца
     */
    @Nullable
    @ApiModelProperty(value = "Имя владельца", example = "Дядя Фёдор")
    private String name;

    /**
     * Телефон владельца
     */
    @Nullable
    @ApiModelProperty(value = "Номер телефона", example = "+7(930)70-60-850")
    private String phone;

    /**
     * Адрес владельца
     */
    @Nullable
    @ApiModelProperty(value = "Адрес", example = "д. Простоквашино")
    private String address;
}
