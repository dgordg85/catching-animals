package ru.kozyrev.spring.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Класс DTO владельцев животных
 */
@Getter
@Setter
@ApiModel(description = "Модель DTO владельца")
public class OwnerDTO extends AbstractDTO {

    /**
     * Поле имя владельца
     */
    @NotNull
    @ApiModelProperty(value = "Имя владельца", required = true, example = "Дядя Фёдор")
    private String name;

    /**
     * Поле телефона владельца
     */
    @Nullable
    @ApiModelProperty(value = "Телефон владельца", required = true, example = "+7(930)70-60-850")
    private String phone;

    /**
     * Поле адреса владельца
     */
    @Nullable
    @ApiModelProperty(value = "Адрес владельца", required = true, example = "д. Простоквашино")
    private String address;
}
