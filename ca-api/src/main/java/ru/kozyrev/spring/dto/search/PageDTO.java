package ru.kozyrev.spring.dto.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Класс ответа на постраничный запрос
 *
 * @param <T> тип объекта для отдачи на фронт OwnerDTO, AnimalDTO etc.
 */
@Getter
@Setter
@AllArgsConstructor
@ApiModel(description = "Модель страницы для передачи данных, а также параметров запроса")
public class PageDTO<T> {

    /**
     * Лист объектов для фронта
     */
    @ApiModelProperty(value = "Данные для передачи на фронт")
    private List<T> data;

    /**
     * Текущая страница
     */
    @ApiModelProperty(value = "Текущая страница", example = "1")
    private long currentPage;

    /**
     * Размер страницы
     */
    @ApiModelProperty(value = "Размер страницы", example = "5")
    private long pageSize;

    /**
     * Общее количество доступных элементов
     */
    @ApiModelProperty(value = "Общее количество элементов", example = "15")
    private long totalElements;
}
