package ru.kozyrev.spring.dto.search;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.spring.util.LocalDateTimeDeserializer;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Класс параметров для поиска Владельца
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Объект для поиска событий")
public class EventSearchDTO {

    /**
     * Поле животное
     */
    @Nullable
    @ApiModelProperty(value = "Идентификатор животного", example = "133bab30-ef4d-45c7-8eed-905835d13d75")
    private UUID animalId;

    /**
     * Поле типа события
     */
    @Nullable
    @ApiModelProperty(value = "Тип события", example = "STERILIZATION")
    private String type;

    /**
     * Время события после
     */
    @Nullable
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @ApiModelProperty(value = "Дата-время фильтрации события После", example = "2017-01-01T23:10:15")
    private LocalDateTime dateAfter;

    /**
     * Время события до
     */
    @Nullable
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @ApiModelProperty(value = "Дата-время фильтрации события До", example = "2019-05-10T11:10:30")
    private LocalDateTime dateBefore;
}
