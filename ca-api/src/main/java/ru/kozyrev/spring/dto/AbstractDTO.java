package ru.kozyrev.spring.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.UUID;

/**
 * Абстрактный класс DTO сущностей
 */
@Getter
@Setter
@ApiModel(description = "Абстрактный DTO")
public abstract class AbstractDTO {

    /**
     * Поле уникального идентификатора
     */
    @Nullable
    @ApiModelProperty(value = "Идентификатор", example = "0f33dfee-6e2b-4ded-8507-1ba95aaea5af", allowEmptyValue = true)
    private UUID id;

    /**
     * Лист хранения ошибок валидации
     */
    @ApiModelProperty(value = "Лист ошибок валидации", allowEmptyValue = true)
    private List<ObjectError> errors;
}
