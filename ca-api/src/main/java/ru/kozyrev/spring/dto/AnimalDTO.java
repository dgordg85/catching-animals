package ru.kozyrev.spring.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.spring.util.LocalDateDeserializer;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Класс DTO животного
 */
@Getter
@Setter
@ApiModel(description = "Модель DTO животных")
public class AnimalDTO extends AbstractDTO {

    /**
     * Поле кличка
     */
    @NotNull
    @ApiModelProperty(value = "Имя животного", required = true, example = "Матроски")
    private String name;

    /**
     * Поле пол
     */
    @NotNull
    @ApiModelProperty(value = "Пол животного", required = true, allowableValues = "MALE, FEMALE", example = "MALE")
    private String gender;

    /**
     * Поле возраст
     */
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @NotNull
    @ApiModelProperty(value = "День рождение животного", required = true, example = "2018-04-15")
    private LocalDate birthday;

    /**
     * Поле описание животного
     */
    @NotNull
    @ApiModelProperty(value = "Описание животного", required = true, example = "Знает всё про колбасу")
    private String description;

    /**
     * Поле типа животного
     */
    @NotNull
    @ApiModelProperty(value = "Тип животного", required = true, allowableValues = "DOG, CAT", example = "CAT")
    private String type;

    /**
     * Поле владелец животного
     */
    @Nullable
    @ApiModelProperty(value = "ID владельца животного", example = "03e955f9-56ef-494d-8caa-e035f4018520")
    private UUID ownerId;

    /**
     * Поле принадлежности к заявке
     */
    @Nullable
    @ApiModelProperty(value = "ID заявки", example = "fa3d926a-6ea0-4716-9f87-af62edf5c852")
    private UUID proposalId;
}
