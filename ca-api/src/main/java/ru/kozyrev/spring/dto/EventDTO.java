package ru.kozyrev.spring.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.spring.util.LocalDateTimeDeserializer;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Класс DTO событий
 */
@Getter
@Setter
@ApiModel(description = "Модель DTO события")
public class EventDTO extends AbstractDTO {

    /**
     * Поле животное
     */
    @NotNull
    @ApiModelProperty(value = "Идентификатор животного", required = true, example = "133bab30-ef4d-45c7-8eed-905835d13d75")
    private UUID animalId;

    /**
     * Поле типа события
     */
    @NotNull
    @ApiModelProperty(value = "Тип события", required = true, example = "STERILIZATION")
    private String type;

    /**
     * Поле времени наступления события
     */
    @NotNull
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @ApiModelProperty(value = "Время наступления события", required = true, example = "2019-05-10T11:10:30")
    private LocalDateTime date;
}
