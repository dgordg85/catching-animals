package ru.kozyrev.spring.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Класс для десериаллизации поля стринговой даты
 */
public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    @Nullable
    public LocalDateTime deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
        final String textDate = parser.readValueAs(String.class);
        return (StringUtils.isNotBlank(textDate)) ? DateFormatUtil.convertStringToLocalDateTime(textDate) : null;
    }
}
