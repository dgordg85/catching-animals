package ru.kozyrev.spring.util;

import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


/**
 * Утилита форматирования даты
 */
public class DateFormatUtil {
    /**
     * Перевод строковой даты в LocalDate
     *
     * @param date дата в формате yyyy-MM-dd
     * @return дата в формате LocalDate
     */
    @Nullable
    public static LocalDate convertStringToLocalDate(@Nullable final String date) {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        final DateTimeFormatter pattern = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, pattern);
    }

    @Nullable
    public static LocalDateTime convertStringToLocalDateTime(@Nullable final String dateTime) {
        if (StringUtils.isBlank(dateTime)) {
            return null;
        }
        final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        return LocalDateTime.parse(dateTime, formatter);
    }
}
