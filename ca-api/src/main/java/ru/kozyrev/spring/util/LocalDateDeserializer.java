package ru.kozyrev.spring.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.time.LocalDate;

/**
 * Класс для десериаллизации поля стринговой даты
 */
public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

    @Override
    @Nullable
    public LocalDate deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
        final String textDate = parser.readValueAs(String.class);
        return (StringUtils.isNotBlank(textDate)) ? DateFormatUtil.convertStringToLocalDate(textDate) : null;
    }
}
